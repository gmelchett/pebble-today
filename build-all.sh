#!/bin/sh

cd utils
./compress.py --begin 1450 --end 1845 --lang sv --target aplite --atleast-one --outdir .. --exclude "till påve"
./compress.py --begin -4000 --end 1920 --lang sv --target basalt --atleast-one --outdir .. --exclude "till påve"
cd ..
pebble build
cp build/*.pbw prebuilt/today-sv.pbw
cd utils
./compress.py --begin 1460 --end 1800 --lang en --target aplite --atleast-one --outdir ..
./compress.py --begin -4000 --end 1860 --lang en --target basalt --atleast-one --outdir ..
cd ..
pebble build
cp build/*.pbw prebuilt/today-en.pbw
cd utils
./compress.py --begin 1900 --end 1950 --lang en --target basalt --atleast-one --outdir ..
cd ..
pebble build
cp build/*.pbw prebuilt/today-en-1900.pbw
cd utils
./compress.py --begin 1951 --end 1997 --lang en --target basalt --atleast-one --outdir ..
cd ..
pebble build
cp build/*.pbw prebuilt/today-en-1951.pbw
