# Today in History - A pebble app

Today in History is pebble watch app that shows what happens in history at this day in time. Event data is stored on the pebble and
no connection to a phone/computer is needed. You can generate a data event set to suite your own prefered era in history.

English and Swedish history events are supported.

### Basalt
![alt text](screenshots/basalt-typical.png "about")
![alt text](screenshots/basalt-typical-sv.png "about")
![alt text](screenshots/basalt-about.png "about")
### Aplite
![alt text](screenshots/aplite-typlical.png "about")
![alt text](screenshots/aplite-about.png "about")

## Usage

Start the app and it will show a random event that occured on this day. Depending on the event data, not all
days have events.

 * Single click select - Wander through today's events
 * Double click select - Shows the about box about what event data the app is built with.

## Prebuilt binaries

Swedish and English apps for aplite, basalt and chalk exists in the prebuilt/ directory.

 * `today-sv.pbw` - Swedish aplite contains events between 1440 and 1850.
 * `today-sv.pbw` - Swedish basalt & chalk contains events between 4000 BC and 1921.
 * `today-en.pbw` - English aplite contains events between 1450 and 1800.
 * `today-en.pbw` - English basalt & chalk contains events between 4000 BC and 1860.
 * `today-en-1900.pbw` - English basalt & chalk contains events between 1900 BC and 1950.
 * `today-en-1951.pbw` - English basalt & chalk contains events between 1951 BC and 1997.

## Event data

All event data comes from Wikipedia. It is downloaded with the utils/get_today_sv.py and utils/get_today_en.py
It requires the python-wikipedia module. Please note that the raw data is manually edited to make the
compression script work! (IMHO too many special case regexp where needed on the raw data.)

## Compression

I did some evaluation of suitable compression algorithm for decompressing compressed text on the pebble.
Huffman and lz-variants all reduced the size with about 1/3 (Among others, [Basic Compression Library](http://bcl.comli.eu/) is a good tool for evaluating.)
Huffman came out with a slight edge. In comparision zlib(gzip) reduced the size with about 1/2 and bzip2 with 2/3.

I initially targeted the aplite platform, which limits the app size 24k for the text+data+bss segments.
That rules out bzip2 and zlib which requires much more memory. Then I came upon [tiny-inflate-library](https://bitbucket.org/jibsen/tinf) and later [uzlib](https://github.com/pfalcon/uzlib)
which implement the zlib RFC and have very timid requirements. The app uses uzlib.
On the aplite platform, each decompressed text must be less than about 8k due to memory contrains. Compressing several
small chunks of data, in comparison with compressing all at once, reduces the efficientcy greatly for zlib.
Therefore for the aplite platform, I use a simple dictionary which replaces common strings with smaller
tokens over all text chunks. (I know, it is possible to use a common dictionary over several compressed chunks, but I didn't 
For the basalt platform with 64k ram limit that is not needed and actually increases the compression size.


## Building
You need obviously the pebble SDK installed and python 3. Before you can build the app, you have
to generate data sets. `./build-all.sh` will generate data sets and build.

It will first build Swedish and then English. The output binaries are copied to `prebuilt/`.

## Changing time period

Have a look at the `build-all.sh` script.

## Licence

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


The data is copyrighed [wikipedia](www.wikipedia.org).

Icon made by [Freepik](http://www.freepik.com/) from [www.flaticon.com](www.flaticon.com).
