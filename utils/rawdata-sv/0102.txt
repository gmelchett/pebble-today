366 – Ett stort antal alemanner korsar den istäckta floden Rhen och inleder därmed en invasion av det romerska kejsardömet. De erövrar de galliska provinserna, samt intar Alsace och stora delar av den schweiziska högplatån.
533 – Sedan Bonifatius II har avlidit året innan väljs Mercurius till påve och tar namnet Johannes II, då han inte vill använda sitt födelsenamn som påvenamn, eftersom Mercurius är en gud i romersk mytologi.
1204 – Dagen efter den norske kungen Håkon Sverressons död utropas hans endast fyraårige sonson Guttorm Sigurdsson till kung av Norge. Denne dör dock själv av sjukdom redan den 11 augusti samma år.
1296 – Upplandslagen stadfästs genom kung Birger Magnussons beslut. Denna lag, som är en revidering av tidigare lagar, har utarbetats av Tiundalands lagman Birger Persson, med hjälp av domprosten Andreas And. I och med lagens stadfästande sammanslås folklanden (Attundaland, Fjädrundaland och Tiundaland samt södra och norra Roden) i Uppland till en gemensam lagsaga.
1492 – Granada i Emiratet av Granada, det sista moriska fästet i Spanien, kapitulerar till Kastilien. I och med detta är reconquistan (de kristnas ”återerövring” av Iberiska halvön från muslimerna) över och den nästan 800-åriga muslimska närvaron i området är slut.
1762 – Svenskarna besegrar preussarna i slaget vid Neu Kahlen. Detta blir det sista fältslaget under det pommerska kriget och direkt därefter återvänder de svenska trupperna till Svenska Pommern och sluter stillestånd med preussarna, vilket varar till fredsslutet den 22 maj samma år.
1788 – Georgia ratificerar den amerikanska konstitutionen och blir därmed den 4:e delstaten som upptas i den amerikanska unionen.
1824 – Tidningen Stockholms Dagblad grundas och utkommer varje vardag. Från 1885 till nedläggningen 1931 utkommer även en söndagsbilaga.
1872 – Den amerikanske mormonledaren Brigham Young blir arresterad för månggifte, då han har 25 hustrur, vilket går emot amerikanska lagar. Knappt 20 år senare tar den mormonska kyrkan avstånd från alla typer av månggifte, bland annat för att dess territorium Utah ska kunna bli en amerikansk delstat.
1925
Det svenska postgirot inleder sin verksamhet med omkring 700 kontoinnehavare och som en del av Postsparbanken. Girot är tänkt att tillgodose samhällets behov av effektivare betalningsförmedlingar genom pengagirering istället för kontanta betalningar.
En kraftig storm drabbar Sverige och bland annat vållas stor förödelse i Göteborg.

1928 – Arbetsgivare inom den svenska pappersmassaindustrin lockoutar 17 000 arbetare och konflikten utvidgas senare med 13 000 arbetare vid pappersbruken.
1942 – Under andra världskriget intar japanerna den filippinska huvudstaden Manila, en vecka efter att den amerikanska militären har dragits bort från staden. Den japanska ockupationen varar i tre år och i början av 1945 förstörs de mesta av staden under den amerikanska återerövringen av den.
1950 – Den 17-årige Ingemar Johansson blir mästare i tungvikt i junior-SM i boxning. Redan under sommaren 1952 ställer han upp i sommar-OS i Helsingfors. I september inleder han sin karriär som professionell boxare.
1955 – Panamas president José Antonio Remón Cantera blir nedskjuten med maskingevär på en kapplöpningsbana i huvudstaden Panama City. Mordet förblir dock olöst.
1974 – Elransonering införs i Sverige på grund av den pågående oljekrisen. Den 8 januari införs även bensinransonering, vilken dock upphör 29 januari, medan elransoneringen varar till 7 mars.
2000 – Stavkyrkan i Skaga i västgötska Karlsborg (en rekonstruktion från 1958 av den medeltida kyrka från 1137, som revs 1826), brinner ned till grunden. I juni året därpå invigs ytterligare en rekonstruerad version av kyrkan.
2003 – Biskop Caroline Krook i Stockholms stift döms till 16 000 kronor i böter för tjänstefel, för att hon har vägrat lämna ut brev som enligt hovrätten är offentlig handling.


