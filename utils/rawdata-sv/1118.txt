1105 – Silvester IV utses till motpåve.
1343 – Fred sluts mellan Sverige och Danmark i Varberg. Magnus Eriksson avstår från territorier väster om Öresund, medan Valdemar Atterdag avstår från Skånelandskapen och bekräftar Sveriges innehav av dem. I freden utnämns också Magnus son Håkan Magnusson formellt till kung av Norge, dock med fadern som förmyndare.
1905 – Det norska stortinget väljer den danske prinsen Carl till ny norsk kung under namnet Håkon VII efter att svensk-norska unionen har upplösts.
1918 – Lettland förklarar sig självständigt från Ryssland.
1928 – Musse Pigg visas för första gången i den tecknade filmen Musse Pigg som ångbåtskalle.
1976 – Spaniens parlament etablerar demokrati efter 37 år diktatur.
1978 – Över 900 medlemmar av sekten Folkets tempel under ledning av Jim Jones begår kollektivt självmord i Guyanas djungel.
1985 – Den tecknade serien Kalle och Hobbe publiceras för första gången i ett antal amerikanska dagstidningar.
1987 – Branden vid King's Cross tunnelbanestation i London där 31 människor omkommer.
1993 – Rockgruppen Nirvana (musikgrupp) gör sin kända Live–spelning, MTV Unplugged in New York. Året före släppte de sitt tredje och sista studioalbum, In Utero.
2003 – Havererar en Super Puma i havet under en räddningsövning vid Rörö utanför Göteborg. 6 av de ombordvarande 7 omkommer.
2005 – Brand i den för reparationer avstängda delen av Katarina södra skola i Stockholm.
2006 – Tom Cruise och Katie Holmes gifter sig.
2010 – Fotbollsspelaren Robert Pires blir klar för den engelska fotbollsklubben Aston Villa.


