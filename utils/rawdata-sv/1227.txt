418 – Efter påven Zosimus död dagen före väljs Eulalius till motpåve.
795 – Sedan Hadrianus I har avlidit två dagar tidigare väljs Leo III till påve.
1831 – Charles Darwin påbörjar sin historiska, och vetenskapligt banbrytande, resa med fartyget H.M.S. Beagle (från Plymouth, England).
1904 – James Barries pjäs Peter Pan har premiär i London.
1934 – Persien byter namn till Iran.
1939 – Erzincan i Turkiet utsätts för en jordbävning.
1945 – Korea delas i en nordlig och en sydlig del.
1945 – Världsbanken grundas av 28 medlemsnationer.
1978 – Spanien blir en demokrati för första gången sedan spanska inbördeskriget.
1983 – Stora delar av Sverige blir strömlöst när en frånskiljare i ett ställverk utanför Enköping lossnar.
1985 – Palestinska terrorister dödar 20 personer på Roms och Wiens flygplatser.
1991 – Gottröraolyckan inträffar.
2002 – Två bilbomber dödar 72 människor och skadar 200 utanför det proryska regeringshögkvarteret i Groznyj, Tjetjenien.
2007 – Oppositionsledaren i Pakistan, Benazir Bhutto, mördas i ett självmordsattentat.


