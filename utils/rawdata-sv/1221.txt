1124 – Sedan Calixtus II har avlidit en vecka tidigare väljs Lamberto Scannabecchi till påve och tar namnet Honorius II.
1872 – Challengerexpeditionen avseglar från Portsmouth på en fyra år lång vetenskaplig expedition som skulle lägga grunden för oceanografin.
1898 – Marie och Pierre Curie upptäcker radium.
1913 – Första korsordet publiceras.
1956 – Den amerikanska medborgarrättsrörelsen får ett genombrott när USA:s högsta domstol slår fast att det strider mot landets grundlag att segregera passagerare på bussar och andra kollektivtransportmedel, mycket tack vare Rosa Parks vägran att ge sin plats åt en vit man på bussen den 1 december 1955.
1958 – General Charles de Gaulle väljs till president i Frankrike.
1965 - Rasdiskrimineringskonventionen antas.
1968 – Apollo 8 sänds upp.
1988 – Libyska terrorister spränger ett plan ovanför Lockerbie, i Skottland, och dödar 270 personer (se Lockerbieattentatet).
1991 – Oberoende staters samvälde skapas efter Sovjetunionens upplösning.
2006 – En ny motorväg på E4 mellan Uppsala och Björklinge öppnas för trafik.
2007 – Estland, Lettland, Litauen, Polen, Tjeckien, Ungern, Slovakien, Slovenien och Malta blir medlemmar av Schengensamarbetet.
2012 – Mayakalenderns innevarande stora årscykel om 5 125 år är till ända.


