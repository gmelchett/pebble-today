
502 f.Kr. – En solförmörkelse inträffar i Egypten (enligt en uträkning; det finns inga nedtecknade ögonvittnesskildringar av händelsen).
771 – Karl den store blir kung av hela Frankerriket.
1110 – Korsfararna erövrar Sidon.
1154 – Sedan Anastasius IV har avlidit dagen innan väljs Nicholas Breakspear till påve och tar namnet Hadrianus IV.
1214 – Vid Vilhelm I:s död efterträds han som kung av Skottland av sin son Alexander II.
1534 – Den turkiska sultanen Süleyman I intar Bagdad.
1563 – Slutsession hålls vid kyrkomötet i Trident.
1619 – De första engelska kolonisatörerna stiger iland i Virginia.
1644 – Fredskongressen i Münster inleds. Denna leder fyra år senare fram till westfaliska freden.
1674 – Fader Jacques Marquette grundar vad som senare blir staden Chicago, Illinois.
1676 – Svenskarna under Karl XI besegrar danskarna under Kristian V i slaget vid Lund, Nordens blodigaste slag.
1783 – General George Washington tar farväl av sina officerare.
1791 – Första utgåvan av The Observer, världens första söndagstidning, kommer ut.
1829 – Britterna förbjuder änkebränning i Indien.
1851 – Ett misslyckat statskuppsförsök genomförs i Frankrike.
1872 – Skeppet Mary Celeste hittas utanför Azorerna, övergivet sedan nio dagar, men endast lätt skadat.
1918 – President Woodrow Wilson seglar till Versailles för fredsöverläggningar efter första världskriget. Han blir därmed den första amerikanske president som reser utomlands under sin ämbetstid.
1930 – Vatikanen godkänner familjeplanering med säkra perioder.
1942 – De första amerikanska bombningarna av italienska fastlandet under andra världskriget genomförs.
1945 – Amerikanska senaten godkänner amerikanskt medlemskap i Förenta nationerna.
1958 – Autonoma Republiken Elfenbenskusten utropas.
1977 – Jean-Bedel Bokassa, president i Centralafrikanska republiken, kröner sig själv till kejsare.
1982 – Kina antar sin grundlag.
1984 – Ett kuwaitiskt flygplan på väg till Pakistan kapas och tvingas flyga till Teheran.
1991 – Journalisten Terry Anderson släpps efter sju år som gisslan i Beirut.
1992
Alexander Grosu blir världsmästare i kroppsbyggning.
Amerikanska soldater sänds på uppdrag till Somalia.

1994 – 4 personer dödas i ett skottdrama vid Sturecompagniet på Stureplan i Stockholm.


