1244 – Kungariket Jerusalem besegras av sultan as-Salih Ayyub under slaget vid la Forbie.
1404 – Sedan Bonifatius IX har avlidit den 1 oktober väljs Cosimo Gentile de' Migliorati till påve och tar namnet Innocentius VII.
1662 – Karl II av England säljer Dunkerque till Frankrike för 40 000 pund.
1723 – Riksdagsordningen 1723 antas i Sverige.
1797 – Freden i Campo Formio sluts mellan Frankrike och Österrike.
1810 – Den första Oktoberfesten hålls i München, Bayern, Tyskland.
1886 – Lundby nya kyrka i Göteborg invigs.
1904 – Moderaterna bildas, då som Allmänna valmansförbundet.
1945 – Överste Juan Perón genomför en statskupp och blir Argentinas ledare.
1961 – Franska regimen massakrerar runt 200 algerier i Paris; Massakern i Paris 1961.
1969 – Uddeholms-koncernen offentliggör att Nykroppa Järnverk ska läggas ned.
1977 – Ett västtyskt specialkommando stormar ett kapat Lufthansaplan som står på flygplatsen i den somaliska huvudstaden Mogadishu, dödar flera av kaparna och befriar gisslan.
1989 – San Francisco, Kalifornien, USA skakas av ett jordskalv med magnituden 7,1 på Richterskalan, med epicentrum i Loma Prieta-bergen söder om staden. 63 människor omkommer och materiella skador för cirka 6 miljarder dollar uppstår.
1992 – Temadagen Internationella dagen för utrotande av fattigdom inrättas av FN.
2004 – På söndagsförmiddagen attackeras Johan Westerlund i Naustaområdet utanför Jokkmokk av en björn och avlider. Senast dessförinnan en slagbjörn dödade en människa i Sverige var 1902.
2006 – USA:s befolkning når 300 miljoner invånare.
2007 – En ny motorväg mellan Björklinge och Mehedeby i norra Uppland invigs.


