1107 – Vid Edgars död efterträds han som kung av Skottland av sin bror Alexander I.
1198 – Sedan Celestinus III har avlidit samma dag väljs Lotario dei Conti di Segni till påve och tar namnet Innocentius III.
1297 – Genuesaren Francesco Grimaldi leder en grupp guelfer, som intar ett furstepalats vid Genuas kust. Strax därefter utropas hans kusin Rainier Grimaldi till furste över ett litet område runt palatset, som får namnet Monaco (efter det italienska ordet för munk), eftersom Francesco vid erövringen lurade sig in i palatset utklädd till franciskanermunk.
1307 – Ett stillestånd sluts i Bogesund mellan den danske kungen Erik Menved och den svenske kungen Birger Magnussons bröder, hertigarna Erik och Valdemar, vilket ska gälla fram till jul samma år. Därmed har hertigarna undanröjt det danska hotet mot sig för nära ett år framåt och de kan koncentrera sig på kampen mot sin bror.
1455 – Påven Nicolaus V utfärdar bullan Romanus Pontifex, där Portugals kung Alfons V tillsammans med Henrik Sjöfararen och deras efterkommande erhåller monopol på handel, sjöfarten och slavhandel på Afrika.
1657 – Georg II Rákóczy av Siebenbürgen (nuvarande Transsylvanien), angriper Polen, i allians med Sverige, som under Karl X Gustavs befäl är i krig med Polen sedan 1655.
1678 – Svenskarna besegrar danskarna i slaget vid Warksow på den tyska ön Rügen under det pågående skånska kriget.
1806 – Kapkolonin (större delen av nuvarande Sydafrika) som har erövrats av britterna från holländarna 1795, för att den inte ska falla i fransmännens händer (då de har ockuperat Nederländerna) har återlämnats till holländarna 1803, men nu återtas den på nytt av britterna, för att Napoleon inte ska kunna utnyttja den. Den blir därmed en permanent brittisk koloni och förblir i brittiska händer fram till 1910, då Sydafrikanska unionen grundas.
1815 – Andrew Jacksons amerikanska trupper besegrar de brittiska styrkorna under sir Edward Michael Pakenham i slaget vid New Orleans, vilket avslutar 1812 års krig mellan Storbritannien och USA.
1867 – Svarta män får rösträtt i det amerikanska huvudstadsterritoriet District of Columbia.
1877 – Lakota-, sioux- och nordcheyenneindianer blir besegrade av det amerikanska kavalleriet i slaget vid Wolf Mountain (i Montanaterritoriet). Detta blir lakotahövdingen Crazy Horses sista strid, då han avlider ett halvår senare.
1912 – Det sydafrikanska socialdemokratiska partiet African National Congress (ANC) grundas under namnet Sydafrikanska ursprungsbefolkningens nationalkongress (South African Native National Congress) för att kämpa för de svartas rättigheter i landet. Först 1994 kommer partiet till makten, då dess dåvarande ledare Nelson Mandela blir Sydafrikas president.
1916 – Västmakterna retirerar från den osmanska Gallipolihalvön under första världskriget, efter ett katastrofalt misslyckat invasionsförsök där.
1917 – Svenska staten beslagtar all brödsäd, mjöl och bröd i landet, för att ransonera dem under de knappa tider som råder under det pågående första världskriget.
1918 – Den amerikanske presidenten Woodrow Wilson presenterar den så kallade Wilsondoktrinen, som består av 14 punkter om hur fredsavtalet efter första världskriget bör utformas. När Versaillesfreden väl sluts sommaren året därpå tar man inte så stor hänsyn till doktrinen, vilket blir en av anledningarna till, att USA aldrig går med i den fredsbevarande internationella organisationen Nationernas förbund, som upprättas i samband med freden.
1924 – Renhållningsarbetarna i Sundsvall går ut i strejk.
1926 – Arabhövdingen Abdul-Aziz ibn Saud utropar sig till kung av Hejaz och ändrar landets namn till Saudiarabien. Vid tillfället består landet av det nuvarande Saudiarabiens kuststräcka vid Röda havet.
1931 – Den svenske författaren Hjalmar Bergman begravs, sedan han har avlidit en vecka tidigare.
1932 – Ett misslyckat bombattentat utförs mot den japanske kejsaren Hirohito i Tokyo.
1937 – Den svenske teologen Olle Meurling rapporteras stupad i Spanien. Han är en av de första svenskar som har rest som frivillig till spanska inbördeskriget för att kämpa mot Franco.
1940
Svenska riksdagen godkänner vissa tvångsmedel i krigstid, såsom exempelvis censur och transportförbud.
Finland besegrar Sovjetunionen i slaget vid Suomussalmi under finska vinterkriget.

1958 – 14-årige Bobby Fisher vinner för första gången amerikanska mästerskapet i schack.
1959
Fidel Castro och hans styrkor står som definitiva segrare i det Kubanska inbördeskriget, då de intar staden Santiago de Cuba (huvudstaden Havanna intogs redan på nyårsdagen samma år).
Charles de Gaulle avgår som Frankrikes premiärminister och blir istället landets förste president under femte republiken. Samtidigt efterträds han på premiärministerposten av Michel Debré.

1960 – Flera antisemitiska aktioner genomförs i Sverige samtidigt som en våg av antisemitiska tilltag sveper över Europa.
1964 – Ett upprop om att kristendomsämnet ska bevaras på gymnasiet, undertecknat av 2,2 miljoner svenskar, överlämnas till den svenska regeringen.
1973 – Rättegång inleds mot de sju män, som står åtalade för att ha placerat ut avlyssningsutrustning i det amerikanska demokratiska partiets högkvarter under presidentvalet året innan, en händelse som går till historien under namnet Watergateskandalen.
1974 – Bensinransonering införs i Sverige under den pågående oljekrisen, sex dagar efter att elransonering har införts.
1975 – Ella Grasso blir guvernör i Connecticut och blir USA:s första kvinnliga guvernör, som inte får posten genom att efterträda sin make.
1986 – Bioteknikföretaget Fermentas vd Refaat El-Sayed och Volvos Pehr G. Gyllenhammar tillkännager att de båda företagen ämnar gå samman.
1991 – Sveriges regering bidrar till FN-insatsen i Kuwaitkriget genom att skicka ett fältsjukhus till Saudiarabien.
1992 – Sakigake blir den första japanska rymdfarkost, som flyger förbi jorden.
2001 – Elfenbenskustens nyvalde president Laurent Gbagbo utsätts för ett misslyckat kuppförsök, då långtifrån alla erkänner honom som vinnare av presidentvalet året innan.
2005 – En stark storm drabbar norra Europa, däribland Sverige, där 100 000-tals hushåll blir ström- och telefonlösa för en lång tid och skador för flera miljoner kronor uppstår.
2006 – Stora delar av den kazakiska regeringen avgår av okänd anledning.
2010 – Togos herrlandslag i fotboll utsätts för en terroristattack när laget i sin spelarbuss är på väg till det afrikanska mästerskapet i Angola. Tre personer ur sällskapet dödas och ett antal skadas.


