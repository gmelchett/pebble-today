1305 – Den skotske frihetskämpen William Wallace avrättas i London.
1328 – Filip VI kröns till kung av Frankrike.
1521 – Kristian II avsätts som svensk kung och Gustav Eriksson (Vasa) väljs till riksföreståndare.
1541 – Den franske upptäcktsresanden Jacques Cartier kommer till en plats nära Québec på sin tredje resa till Kanada.
1727 – Kina och Ryssland sluter fördraget i Kjachta.
1741 – Svenskarna blir besegrade av ryssarna i slaget vid Villmanstrand under hattarnas ryska krig.
1813 – De allierades nordarmé besegrar en fransk armé i slaget vid Grossbeeren under napoleonkrigen.
1921 – Kungariket Irak utropas.
1927 – Sacco och Vanzetti avrättas i elektriska stolen efter en mycket uppmärksammad rättegång.
1939 – Sovjetunionen och Tyskland ingår icke-angreppsavtalet Molotov-Ribbentrop-pakten, vilket hjälper Sovjet ut ur den cordon sanitaire landet har befunnit sig i alltsedan revolutionen.
1942 – Tyska trupper når under andra världskriget fram till Stalingrad.
1973 – Norrmalmstorgsdramat inleds då en maskerad man tar fyra banktjänstemän som gisslan i ett bankvalv i Nordbankens kontor på Norrmalmstorg i Stockholm.
2006 – Österrikiska Natascha Kampusch lyckas rymma efter att ha suttit fången hos sin kidnappare i åtta år. Kidnapparen Wolfgang Přiklopil begår då självmord.
2010 – Den reguljära trafiken på Spårväg City i Stockholm startar.


