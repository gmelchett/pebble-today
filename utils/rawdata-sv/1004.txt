1562 – Hertig Johan (från 1568 Johan III) gifter sig med polska prinsessan Katarina Jagellonica i Vilnius.
1582 – Den julianska kalendern används för sista dagen i vissa katolska länder, eftersom påven Gregorius XIII har bestämt att den gregorianska kalendern ska användas i stället. Detta leder till att man hoppar över den 5–14 oktober detta år. Det dröjer dock till början av 1900-talet, innan hela världen har övergått till den nya stilen, som den nya kalendern också kallas.
1703 – Under stora nordiska kriget tvingas den starkt befästa polska staden Thorn efter en belägring kapitulera till Karl XII.
1904 – IFK Göteborg grundas.
1908 – Österrike-Ungern annekterar Bosnien och Hercegovina.
1910 – I Portugal inleder republikanska grupper ett uppror.
1925 – Den finländska torpedbåten S 2 förliser och tar med sig 54 man i djupet.
1957 – Sovjetunionen sänder upp Sputnik 1, den första konstgjorda satelliten runt jorden.
1966 – Kungariket Lesotho blir självständigt från Storbritannien.
1970 – Janis Joplin hittas död efter en överdos, 27 år gammal.
1983 – En demonstration i Stockholm mot löntagarfonderna samlar cirka 75 000 människor, vilket gör det till en av de största demonstrationerna i Sverige genom tiderna.
1992 – Samajwadi Party, ett av Indiens största partier grundas.
1999 – Nordiska samfundet mot plågsamma djurförsök byter namn till Djurens rätt.


