384 – Sedan Damasus I har avlidit denna dag väljs Siricius till påve (denna dag eller 15, 22 eller 29 december).
1205 – Biskop John Grey av Norwich väljs till ärkebiskop av Canterbury.
1317 – Nyköpings gästabud äger rum på Nyköpingshus.
1701 – Sverige erövrar fästningen Dünamünde utanför Riga.
1816 – Indiana blir den 19:e delstaten att upptas i den amerikanska unionen.
1911 – Roald Amundsens expedition når sydpolen. Det är första gången som människor gör ett dokumenterat besök på sydpolen.
1936 – Edvard VIII abdikerar från den brittiska tronen för att kunna gifta sig med den frånskilda amerikanskan Wallis Simpson, då reglerna och parlamentet inte godkänner att han förblir kung, om han gifter sig med henne. Han efterträds samma dag som kung av Storbritannien av sin bror Georg VI.
1937 – Italien lämnar Nationernas Förbund.
1941 – Tyskland och Italien förklarar krig mot USA.
1946 – Förenta Nationernas Barnfond (United Nations International Children Emergency Fund; Unicef) grundas.
1972 – Månlandaren Apollo 17 landar på månen.
2001
En razzia genomförs mot piratkopieringsorganisationen DrinkOrDie.
Kina går med i World Trade Organization.

2010 – En självmordsbombare i centrala Stockholm dödar sig själv och två personer skadas, när hans bomb detonerar.


