480 f.Kr. – Perserna plundrar Aten, vars invånare flyr till Salamis och Peloponnesos.
687 – När påven Konon avlider utses Theodorus till motpåve.
1619 – Alingsås får stadsprivilegium av Gustav II Adolf.
1676 – Sedan Clemens X har avlidit den 22 april väljs Benedetto Odescalchi till påve och tar namnet Innocentius XI.
1745 – Karl Edvard Stuart, kallad Bonnie Prince Charlie och hans jakobitiska armé besegrar engelsmännen i slaget vid Prestonpans i Skottland.
1792 – Det revolutionära nationalkonventet i Frankrike röstar igenom avskaffandet av monarkin.
1860 – Slaget vid Palikao där den allierade segern öppnade vägen för en invasion av Peking och sedermera seger i det andra opiumkriget.
1957 – Vid Håkon VII:s död efterträds han som kung av Norge av sin son Olav V.
1994 – Första avsnittet av tv-serien Tre Kronor sänds.
2010 – Burj Khalifa står klart som världens högsta torn.


