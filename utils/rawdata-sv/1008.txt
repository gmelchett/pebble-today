451 – Konciliet i Chalcedon inleds, vid vilket de orientaliskt ortodoxa kyrkorna bryter med de övriga kristna samfunden.
1856 – De kinesiska myndigheterna i Guangzhou bordar ett kinesiskt piratfartyg som seglat under engelsk flagg. Detta blir upptakten till det andra opiumkriget.
1871 – Den stora chicagobranden, som ödelägger stora delar av Chicago, utbryter.
1895 – Koreas drottning Min Myongsong blir mördad i ett attentat
1908 – Det susar i säven (The Wind in the Willows) av Kenneth Grahame publiceras.
1916 – Efter totalt 0–24 i fem möten kommer den första svenska segern i fotboll mot Danmark, 4–0
1958 – Den svenske kirurgen Åke Senning opererar för första gången in en pacemaker
1965 – Den för sin tid högsta byggnaden i England, det 188 meter höga Post Office Tower, invigs.
1973 – Londons första legala kommersiella radiostation LBC har premiär. Inriktningen är diskussionsprogram och nyheter.
2001
En flygolycka i Milano inträffar med 118 omkomna.
USA anfaller Afghanistan, vilket benämns ”självförsvarsaktion”, men amerikanska styrkor besätter ändå landet vilket mer visar att det är en regelrätt invasion.

2004 – Ny motorväg på E4 förbi Örkelljunga och Skånes Fagerhult öppnas för trafik.
2005 – Jordbävningen i Kashmir 2005.
2008
På den sjunde årsdagen av USA:s anfall mot Afghanistan leder den förre svenske försvarsministern Thage G. Peterson ett fackeltåg i Stockholm, till minne av de afghaner som har dött sedan USA:s invasion.
Stockholmsbörsen (OMXS) sjunker med 6,15 procent (Läs vidare i Finanskrisen 2008).


