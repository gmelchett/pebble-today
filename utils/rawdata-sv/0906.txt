1492 – Christofer Columbus avseglar från Kanarieöarna på sin första resa västerut.
1522 – Juan Sebastián de Elcano avslutar den första världsomseglingen då fartyget Victoria återkommer till Spanien.
1813 – Slaget vid Dennewitz, där den allierade nordarmén under ledning av Sveriges kronprins Karl Johan besegrar fransmännen under marskalk Michel Ney.
1901 – Den amerikanske presidenten William McKinley skjuts av anarkisten Leon Czolgosz, och dör av skadorna en dryg vecka senare, den 14 september.
1914 – Första Marne-slaget börjar. I striderna deltar cirka 2 miljoner soldater och cirka 100 000 stupar eller såras i ett slag, som betecknas som framgångsrikt för de allierade.
1936 – Den sista överlevande pungvargen, även kallad tasmansk varg, avlider på Hobart Zoo i Tasmanien.
1944 – Anne Frank och hennes familj anländer till Auschwitz.
1968 – Swaziland blir självständigt från Storbritannien.
1970 – Palestinska PFLP kapar fyra passagerarplan; från El Al, Pan American, Swissair och TWA.
1987 – Patrik Sjöberg blir världsmästare i höjdhopp.
1991 – Sovjetunionen erkänner de baltiska staternas självständighet.
1997 – Uranus måne Caliban upptäcks.


