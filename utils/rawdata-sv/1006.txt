105 f.Kr. – Cimbrerna krossar två romerska arméer i slaget vid Arausio vid Rhône, vilket är det värsta romerska nederlaget sedan slaget vid Cannae 111 år tidigare.
891 – Sedan Stefan V har avlidit den 14 september väljs Formosus till påve.
1689 – Sedan Innocentius XI har avlidit den 12 augusti väljs Pietro Vito Ottoboni till påve och tar namnet Alexander VIII.
1908 – Österrike-Ungern annekterar Bosnien-Hercegovina.
1927 – Urpremiär i USA för den första delvis ljudlagda långfilmen, Jazzsångaren.
1973 – Egypten anfaller Israel i Oktoberkriget.
1981 – Egyptens president Anwar Sadat mördas i ett attentat.
1985 – Marita Koch sätter nytt världsrekord på 400 m slätt i friidrott med tiden 47,60 s i Canberra.
1995 – Den första exoplaneten upptäcks runt stjärnan 51 Pegasi.
2007 – Barbro Beck-Friis, Kerstin Ekman och Annika Åhnberg promoveras till hedersdoktorer vid Sveriges lantbruksuniversitet.
2008 – Stockholmsbörsen (OMXS) faller 7,12 procent efter oron från Island, där alla isländska banker handelsstoppades. Isländska FI tvingar bankerna att börja sälja tillgångar utomlands. Läs vidare i Finanskrisen 2008.


