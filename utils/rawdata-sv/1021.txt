686 – Sedan Johannes V har avlidit den 2 augusti väljs Konon till påve.
1032 – Sedan Johannes XIX har avlidit dagen innan väljs Theophylactus av Tusculum till påve och tar namnet Benedictus IX.
1368 – Påven Urban V möter kejsar Karl IV i Rom och den heliga Birgitta kan till slut be påven godkänna hennes orden.
1422 – Vid Karl VI:s död utropas hans son Karl VII till kung av Frankrike i Bourges, medan den nytillträdde engelske kungen Henrik VI utropas till kung av Frankrike i Paris.
1520 – Ferdinand Magellan upptäcker ett sund som senare kommer att kallas Magellans sund som skiljer Eldslandet från fastlandet i Sydamerika.
1600 – Slaget vid Sekigahara, det mest legendariska slaget i japanska historien, då Tokugawa Ieyasu vinner en stor seger och kan ena landet under Tokugawashogunatet.
1805 – Slaget vid Trafalgar utanför Spaniens kust; amiral Lord Horatio Nelson stupar ombord på HMS Victory, men britterna besegrar fransmännen och Trafalgar går till historien som en av brittiska flottans största segrar genom tiderna.
1862 – USA erkänner Liberia.
1888 - Socialdemokraterna i Schweiz grundas.
1919 – Republiken Österrike utropas.
1937 – Roberto Ortiz väljs till Argentinas president.
1959 – konstmuseet Guggenheim Museum signerat Frank Lloyd Wright invigs i New York, USA.
2001 – Hammarby IF säkrar sitt första SM-guld i fotboll då man slår Örgryte hemma på Söderstadion med 3–2.


