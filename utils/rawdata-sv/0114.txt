1357 – Stilleståndet i Lödöse sluts mellan den svenske kungen Magnus Eriksson och hans son Erik, som året innan har utropat sig till kung i opposition mot fadern. Kriget mellan dem avstannar därmed tillfälligt, men trots att en fred sluts mellan dem den 28 april återupptas inbördeskriget om kronan snart igen.
1509 – Ett unionsmöte hålls mellan Sverige och Danmark i Helsingborg. Man misslyckas dock med förhandlingarna om Kalmarunionens återupprättande.
1766 – Vid Fredrik V:s död efterträds han som kung av Danmark och Norge av sin son Kristian VII.
1814 – Freden i Kiel sluts mellan Sverige och Danmark. Danmark överlåter Norge till Sverige, i utbyte mot Svenska Pommern, men norrmännen vägrar acceptera fredsbestämmelserna och utropar den 17 maj samma år Norge till ett självständigt rike.
1907 – Ett jordskalv drabbar Jamaicas huvudstad Kingston, varvid cirka 1 000 människor omkommer.
1912 – Den svenske kyrkomannen Manfred Björkquist startar den så kallade pansarbåtsinsamlingen, för att allmänheten ska bekosta byggandet av en pansarbåt till den svenska flottan, då den svenska regeringen vill rusta ner och just har beslutat att ställa in byggandet av båten.
1925 – Radioprogrammet Barnens brevlåda börjar sändas i svensk radio med Sven Jerring (som i programmet blir känd som ”Fabror Sven”) som programledare. När programmet läggs ner 1972 är det världens äldsta radioprogram.
1926 – I Stockholm undertecknas en konvention angående hur tvister mellan Sverige och Danmark ska avgöras på fredlig väg.
1952 – Det amerikanska tv-programmet The Today Show, som blir världens första TV-morgonprogram, har premiär på tv-kanalen NBC.
1963 – Film- och teaterregissören Ingmar Bergman utnämns till chef för den svenska nationalscenen Dramaten i Stockholm efter att Karl Ragnar Gierow har valt att avgå.
1972 – Vid den danske kungen Fredrik IX:s död efterträds han på tronen av sin dotter Margrethe II, som därmed blir regerande drottning av Danmark.
1973 – Den amerikanske rockartisten Elvis Presley ger konserten Aloha from Hawaii, som även sänds direkt via satellit. Sändningen slår rekord som den mest sedda tv-sändningen någonsin av en enskild artist, med en publik på ungefär en och en halv miljard människor över hela världen.
1988 – Den första hjärttransplantationen med en svensk donator genomförs vid Sahlgrenska Universitetssjukhuset i Göteborg.
1993 – Den polska färjan M/S Jan Heweliusz förliser på Östersjön genom att i hårt väder slå runt. Av de 63 ombordvarande omkommer 54, varav 6 svenskar.
2005 – Den europeiska rymdsonden Huygens landar på planeten Saturnus måne Titan, för att samla data från dess yta.
2007 – Den kraftiga orkanen Per sveper in över södra Sverige och blir landets kraftigaste oväder sedan orkanen Gudrun två år tidigare.
2011 – Tunisiens president Zine El Abidine Ben Ali flyr landet efter de våldsamma protester som riktas mot hans styre och som har inletts 17 december året innan.


