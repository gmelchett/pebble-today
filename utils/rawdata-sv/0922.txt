530 – Sedan Felix IV har avlidit samma dag väljs Bonifatius II till påve, medan Dioskurus väljs till motpåve, men själv avlider redan 14 oktober.
1217 – Den franske kronprinsen Ludvig Lejonet, som av oppositionella stormän året innan har utropats till kung av England, undertecknar fördraget i Lambeth, där han erkänner, att han aldrig har varit legitim härskare över England. Därmed är Henrik III nu Englands obestridde kung.
1503 – Sedan Alexander VI har avlidit den 18 augusti väljs Francesco Todeschini Piccolomini till påve och tar namnet Pius III. Han avlider dock efter endast en månad på posten.
1792 – Det franska nationalkonventet deklarerar den Första republiken.
1932 – Hejaz och Nejd går samman och bildar Kungariket Saudiarabien.
1944 – De allierade segrarmakterna upprättar en kontrollkommission i Helsingfors, Finland.
1955 – Storbritannien får kommersiell tv när ITV inleder sina sändningar.
1960 – Sudanesiska republiken byter namn till Mali.
1984 – Det amerikanska barnprogrammet Fragglarna har svensk premiär i SVT.
1986 – Komediserien ALF om utomjordingen med samma namn har premiär på NBC.
1994 – Komediserien Vänner har premiär på tv-nätverket NBC i USA.
2004 – Dramaserien Lost har premiär på tv-nätverket ABC i USA.


