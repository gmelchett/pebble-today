43 f.Kr. – Andra triumviratet mellan Octavianus (sedermera kejsar Augustus), Lepidus och Marcus Antonius bildas i romerska riket.
579 – Sedan Benedictus I har avlidit den 30 juli väljs Pelagius II till påve.
1751 – Adolf Fredrik kröns till Sveriges kung i Storkyrkan i Stockholm.
1778 – Kapten James Cook blir den första europé som besöker ön Maui på Hawaii.
1832 – Den första spårvagnslinjen i New York invigs, sträckan Prince Street–14:e gatan.
1867 – Mrs Lily Maxwell från Manchester röstar i det brittiska parlamentsvalet. Hon har hamnat i röstlängden av misstag och får eskorteras av vakter som skyddar henne från att lynchas av motståndare till kvinnlig rösträtt.
1924 – Folkrepubliken Mongoliet utropas.
1942 – Filmen Casablanca har premiär i USA.


