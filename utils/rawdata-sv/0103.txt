936 – Sedan Johannes XI har avlidit i december året innan väljs Leo VII till påve.
1322 – När den franske kungen Filip den långe avlider 29 år gammal efterträds han på tronen av sin bror Karl den sköne. Denne blir den siste av den capetingiska ätten, som har styrt Frankrike sedan 987, och vid hans död 1328 efterträds han av sin kusin Filip den lyckosamme, som då grundar kungaätten Valois.
1521 – Påven Leo X utfärdar bullan Decet Romanum Pontificem, genom vilken han bannlyser reformatorn Martin Luther, sedan denne den 10 december året innan har bränt påvens tidigare bulla Exsurge Domine, där han uppmanas ta avstånd från sina reformationsidéer.
1777 – Under det amerikanska revolutionskriget intar George Washingtons trupper Nassau Hall på universitetet i Princeton och besegrar sedan britterna under Charles Cornwallis i det efterföljande slaget vid Princeton.
1888 – Den amerikanske uppfinnaren Marvin Stone tar patent på världens första sugrör, gjort av papper.
1910 – Ebbe Lieberath grundar organisationen Riddarpojkarna i Göteborg, vilken blir grunden till det som senare blir Svenska Scoutförbundet, två år efter att Robert Baden-Powell har grundat scoutrörelsen i Storbritannien.
1925 – Den italienske fascistledaren Benito Mussolini, som har varit Italiens premiärminister sedan 1922, tar på sig ansvaret för den politiska oro, med våldsamheter som följd, som har rått i landet en tid. Han lovar att få bukt med oron och våldsyttringarna och genom att han nu får parlamentets stöd blir han i praktiken Italiens diktator. Med hjälp av sina fascistiska svartskjortor börjar han snart avskaffa de demokratiska institutionerna i Italien.
1947 – Sju år efter att nylonstrumpor har börjat säljas i USA kommer de första exemplaren (27 660 par) till Sverige med flyg från New York. Även om det inte blir upploppsstämning, som i USA, uppstår ändå tidvis kaos, när kvinnor kämpar om de nya strumporna.
1951 – Den svenska riksdagen tillsätter en utredning om att införa television i Sverige. Den 29 oktober 1954 inleds försökssändningar och 1956 börjar man med reguljära svenska tv-sändningar.
1959 – Alaska blir den 49:e delstaten som upptas i den amerikanska unionen. Detta blir den näst sista, då man efter Hawaiis inträde som den 50:e staten i augusti samma år inte har upptagit några nya stater i unionen.
1962 – Påven Johannes XXIII bannlyser den kubanske premiärministern och ledaren Fidel Castro på grund av hans förföljelser av kyrkopersoner. Detta sker endast tre år efter att Castro och kommunisterna har tagit makten på Kuba.
1973 – Den amerikanska hårdrocksgruppen Kiss, som är känd för att medlemmarna alltid uppträder kraftigt svart- och vitsminkade, grundas i New York.
2004 – Den amerikanska rymdstyrelsen NASA:s forskningsfordon Spirit landar framgångsrikt på planeten Mars. Dess uppdrag att göra observationer av planeten är tänkt att pågå i 90 dagar, men det fortsätter fram till 2010, då man förlorar kontakten med det.


