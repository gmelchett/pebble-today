306 – Maxentius, son till före detta kejsaren Maximianus, utropas till kejsare.
312 – Konstantin den store besegrar Maxentius i slaget vid Pons Mulvius och blir därmed den ende romerske kejsaren i väster.
969 – Antiokia faller under Bysans.
1061 – Honorius II utses till motpåve.
1216 – Sedan Johan har avlidit en vecka tidigare efterträds han som kung av England och herre över Irland av sin son Henrik III. Tronpretendenten Ludvig Lejonet gör honom rangen stridig till året därpå, då denne avsäger sig anspråken på den engelska tronen.
1492 – Christofer Columbus landstiger på Kuba.
1704 – 3 000 svenska kavallerister ledda av Karl XII besegrar en sachsisk styrka i slaget vid Punitz.
1707 – En jordbävning inträffar i Japan.
1793 – Eli Whitney ansöker om patent för sin bomullsgin.
1820 – Sveriges första sparbank öppnas av Eduard Ludendorff i Göteborg.
1886 – Frihetsgudinnan invigs i New Yorks hamninlopp.
1908 – Klubben Örebro SK grundas på Trädgårdsgatan 28 i Örebro
1918 – Tjeckoslovakien blir självständigt från Österrike-Ungern.
1922 – Marschen mot Rom av italienska fascister. Mussolini utses till regeringschef.
1940 – Italien invaderar Grekland.
1950 – Nio personer omkommer då en rälsbuss kolliderar med ett persontåg på sträckan Godegård–Mariedamm.
1958 – Sedan Pius XII har avlidit den 9 oktober väljs Angelo Giuseppe Roncalli till påve och tar namnet Johannes XXIII.
1962 – Kubakrisen tar slut, när Sovjetunionens ledare Nikita Chrusjtjov meddelar att de sovjetiska missilerna ska tas bort från Kuba.
1966 – Sveriges sista träkolsblästermasugn i Svartå, Degerfors läggs ned.
1972 – Snabbmatskedjan McDonald's öppnar sin första restaurang i Sverige på Kungsgatan i Stockholm.
1981 – Den sovjetiska ubåten U 137 upptäcks i Gåsefjärden utanför Karlskrona, där den har gått på grund föregående kväll.
2004 – Det Kvindelige Velgørende Selskab i Danmark upplöses.
2007
IFK Göteborg besegrar Trelleborg med 2–0 inför 43 000 åskådare. Göteborgslaget vinner därmed årets Allsvenska och detta blir dess 18:e SM-guld.
SAS VD, Mats Jansson meddelar att alla SAS Dash 8-Q400 permanent ska tas ur trafik.

2013
Stormen Simone drar in över Brittiska öarna och andra delar av Europa, däribland Sverige.
Malmö FF slår IF Elfsborg på Borås Arena med 2–0 och tar sin 20:e allsvenska serieseger. Matchen hotas av stormen Simone, men blir av eftersom domaren Jonas Eriksson inte bedömer att publiken eller spelarna kommer utsättas för fara.


