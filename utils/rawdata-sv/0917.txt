654 – Den avsatte påven Martin I anländer till Konstantinopel, varifrån han skickas i exil av kejsar Konstans II.
936 – Ärkebiskop Unni avlider i Birka när han åter försöker predika kristendomen bland svearna. Detta är det äldsta kända datumet i Sveriges historia.
1179 – Helgonet Hildegard av Bingen dör.
1605 – Slaget vid Kirkholm, där den svenska armén under Karl IX:s befäl lider ett svårt nederlag mot polackerna under Jan Karol Chodkiewicz.
1809 – Freden i Fredrikshamn mellan Sverige och Ryssland undertecknas, vilket avslutar finska kriget 1808–09. Hela Finland, Åland och det dåvarande Västerbotten fram till Torne älv tillfaller Ryssland.
1888 – Brunei ställs under brittiskt beskydd.
1862 – Slaget vid Antietam i amerikanska inbördeskriget.
1894 – Slaget vid Yalu under kriget mellan Japan och Kina 1894–1895, ett sjöslag i vilket den kinesiska flottans huvudstyrka besegras i grund av den japanska.
1930 – Den kurdiska Araratrepubliken i norra Kurdistan slås ner av turkiska flygvapnet
1938 – Andra EM i friidrott inleds i Wien (det första där damtävlingar var tillåtna även om herrtävlingen inledd 3 september i Paris var separat)
1939 – Sovjetunionen anfaller Polen.
1941 – 33 man döda och 17 skadade när tre svenska jagare Klas Horn, Klas Uggla och Göteborg av en olyckshändelse exploderar på Hårsfjärden i Stockholms skärgård.
1944 – Operation Market Garden inleds. Det är kodnamnet på en operation på västfronten under andra världskriget utarbetad huvudsakligen av fältmarskalk Bernard Montgomery. Dess syfte är att snabbt få ett slut på kriget genom ett koncentrerat anfall in i själva Tyskland.
1948 – Greve Folke Bernadotte, FN-medlare från Sverige, mördas i Jerusalem av medlemmar i den sionistiska gruppen Lehi.
1978 – Camp David-avtalet undertecknas av den egyptiske presidenten Anwar Sadat och den israeliske premiärministern Menachem Begin.
1988 – De XXIV olympiska sommarspelen i Seoul, Sydkorea, invigs.
1983 – Barnkanalen Cartoon Network ägd av Ted Turner har Sverige- och Europapremiär med tecknade serier som Familjen Flinta och Jetsons dubbade till svenska.
2006 – I Sverige hålls riksdagsval, där den borgerliga fyrpartikoalitionen Allians för Sverige besegrar Socialdemokraterna och därmed kan bilda den första borgerliga regeringen i landet sedan 1994.


