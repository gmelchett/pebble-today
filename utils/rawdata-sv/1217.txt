497 f.Kr. – Ett tempel till guden Saturnus ära invigs i Rom.
283 – Sedan Eutychianus har avlidit den 7 december väljs Gajus till påve.
1066 – Edgar den fredlöse avsäger sig den engelska tronen, till förmån för Vilhelm Erövraren, som kröns en vecka senare.
1903 – Bröderna Wright (Orville Wright och Wilbur Wright) genomför den första lyckade luftfärden med ett flygplan, i Kitty Hawk, North Carolina, USA.
1951 – Den svenska filmen Hon dansade en sommar har premiär och blir en stor världssuccé.
1969 – Amerikanska flygvapnet meddelar att dess undersökningar inte har gett några bevis för utomjordiska farkoster.
1970
Kravaller i den polska staden Gdynia leder till massaker på varvsarbetare.
Sverige utfärdar lagen om husligt arbete för husligt anställda, som från den 1 juli 1971 ersätter hembiträdeslagen från 1944.

1973 – American Psychiatric Association tar bort homosexualitet från sin lista över mentala sjukdomar.
1974 – Ingemar Stenmark vinner sin första världscupseger i Madonna di Campiglio.
1975 – Lynette Alice Fromme döms i Sacramento, Kalifornien, till livstids fängelse för mordförsök på president Gerald Ford.
1989 – Det första avsnittet av humorserien Simpsons sänds på Fox i USA.
2003
Nya Arlanda invigs av Ulrica Messing.
Preem köper Norsk Hydros 25-procentiga andel i Preemraff Lysekil och äger därmed hela raffinaderiet.


