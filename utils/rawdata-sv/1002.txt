1919 – USA:s president Woodrow Wilson drabbas av slaganfall, blir delvis förlamad och sängbunden och återhämtar sig aldrig helt. Frun Edith assisterar maken och spelar en viktig roll i Wilsonadministrationen under återstoden av mandatperioden, då vicepresident Marshall aldrig formellt övertar presidentämbetet.
1928 – Carl Gustaf Ekman avgår som svensk statsminister och efterträds av Allmänna valmansförbundets Arvid Lindman.
1942 – Den brittiska kryssaren Curacao sjunker utanför Irland efter att ha kolliderat med oceanfartyget Queen Mary, varvid 338 personer omkommer.
1950 – Serien Snobbens första avsnitt publiceras.
1958 – Guinea blir självständigt från Frankrike.
1968 – Ett stort antal demonstranter dödas av militär i Tlatelolcomassakern i Mexiko, tio dagar före invigningen av Olympiska spelen i Mexico City.
1997 – Amsterdamfördraget undertecknas.
2006 – Per Westerberg väljs till svenska riksdagens talman.


