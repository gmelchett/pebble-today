331 f.Kr. – Alexander av Makedonien segrar i slaget vid Gaugamela (nära antikens Nineveh) över den persiske kungen Dareios III. Dareios vänder om sin stridsvagn och flyr, även om hans underlydande fortsätter att kämpa. Alexander följer efter de besegrade persiska styrkorna till Arbela, men Dareios flyr med sitt baktriska kavalleri och grekiska legosoldater in i Medien.
366 – Sedan Liberius har avlidit en vecka tidigare väljs Damasus I till påve. Romare som är missnöjda med detta val väljer istället Ursinus till motpåve.
959 – Vid Edwy den rättvises död efterträds han som kung av England av sin bror Edgar.
965 – Sedan Leo VIII har avlidit den 1 mars väljs Giovanni Crescenzi till påve och tar namnet Johannes XIII.
1536 – Kung Gustav Vasa gifter sig med Margareta Eriksdotter (Leijonhufvud) i Uppsala.
1559 – Prinsessan Katarina Vasa gifter sig i Stockholm med Edzard II av Ostfriesland, vilket sedermera följs av Vadstenabullret.
1779 – Gustav III grundar staden Tammerfors.
1843 – Den brittiska söndagstidningen News of the World kommer ut för första gången.
1858 – Husagan avskaffas i Sverige. Myndiga anställda får inte längre agas, medan manliga under arton år och kvinnliga under sexton år fortfarande tillåts agas (fram till 1920).
1867 – Järnvägen mellan Arboga och Köping invigs. Denna var en del av den planerade Köping-Hults Järnväg.
1888 – Katarina södra skola på Södermalm, Stockholm står färdig för inflyttning.
1906 – Allmän rösträtt införs i Finland.
1908 – Henry Ford introducerar T-Forden, sin tids mest populära och billigaste bil.
1918 – Järnvägsolyckan i Getå.
1946 – Domarna avkunnas i Nürnbergprocessen.
1949 – Kommunisterna och Mao Zedong tar makten i Kina och Folkrepubliken Kina utropas.
1950 – Tunnelbanan i Stockholm börjar trafikera sträckan Slussen–Hökarängen.
1951 – Socialdemokraterna och Bondeförbundet ingår regeringskoalition.
1955 – Sverige avskaffar det brattska motbokssystemet.
1960 – Nigeria blir självständigt från Storbritannien.
1963 – Nnamdi Azikiwe blir Nigerias första president.
1965 – Lophtet faller i Teknologkåren vid Lunds Tekniska Högskolas ägo.
1965 – Mellanöl börjar säljas i vanliga livsmedelsbutiker i Sverige.
1966 – Krigsförbrytarna Albert Speer och Baldur von Schirach friges efter att ha avtjänat sina straff.
1969 – Concorde 001 bryter ljudvallen för första gången under en provflygning i Frankrike.
1971 – Disney World öppnar i Florida.
1978 – Tuvalu blir självständigt från Storbritannien.
1991 – 10-kronan återintroduceras i det svenska myntsystemet.
1992 – Barnkanalen Cartoon Network ägd av Ted Turner har världspremiär med Familjen Flinta och Jetsons.
1993 – Den kommersiella radion premiärsänder i Sverige när NRJ som första station lanseras på egen frekvens i Stockholm.
2010 – De svenska 50-öringarna blir ogiltiga som betalningsmedel.
2017 - Folkomröstningen om självständighet i Katalonien (2017) är.


