269 – Sedan Dionysius har avlidit året innan väljs Felix I till påve.
1066 – Sedan Edvard Bekännaren har avlidit denna eller föregående dag blir Harald Godwinson kung av England och kröns dagen därpå.
1463 – Författaren François Villon benådas från sin dödsdom av det franska parlamentet och döms istället till förvisning från Paris. Hans vidare öden är okända.
1477 – Lorrainarna besegrar burgunderna i slaget vid Nancy, varvid den burgundiske befälhavaren Karl den djärve stupar. Detta leder till att de burgundiska krigen, som har varat sedan 1474, tar slut och Burgund blir en del av Frankrike.
1554 – Den nederländska staden Eindhoven drabbas av en stor brand, som lägger tre fjärdedelar av staden i aska. Den byggs dock snart upp igen.
1675 – Frankrike besegrar Brandenburg och Österrike i slaget vid Colmar i Alsace.
1677 – Svenska trupper intar ett norskhållet blockhus vid bohuslänska Strömstad, men den svåra vintern hindrar vidare trupprörelser för tillfället, varför norrmännen fortsätter att hålla staden.
1719 – Den svenska upphandlingsdeputationens ledamöter avskedas och dess chef Georg Heinrich von Görtz förlorar sin maktställning i svenska staten.
1757 – Den franske kungen Ludvig XV överlever ett mordförsök av Robert-François Damiens. Efter förhör med tortyr blir Damiens den siste i Frankrike att avrättas med så kallat skärpt dödsstraff.
1769 – Den brittiske uppfinnaren James Watt får patent på sin ångmaskin.
1781 – Under det amerikanska revolutionskriget bränns staden Richmond i Virginia ner av brittiska sjöstyrkor ledda av Benedict Arnold.
1813 – Danmarks stora krigsutgifter under napoleonkrigen leder till, att landet nu tvingas genomföra en pengareform och stark nedskrivning av valutans värde, vilket går till historien under namnet Statsbankrutten.
1814 – Det danska fästet Glückstadt kapitulerar för svenskarna under det sjätte koalitionskriget och därmed är hela Holstein i svenska händer. Detta ger en god utgångspunkt i de följande fredsförhandlingarna med Danmark och i freden den 14 januari tvingas Danmark avstå Norge till Sverige.
1878 – Den färöiska tidningen Dimmalætting börjar utges regelbundet, sedan ett första provnummer har utkommit den 8 december året innan.
1895 – Den judiske franske officeren Alfred Dreyfus, som står anklagad för högförräderi, döms till livstids fängelse på den franska kolonin Djävulsön. Egentligen är han oskyldig och under flera år arbetar starka krafter i samhället på att få honom frikänd. Det hela går till historien under namnet Dreyfusaffären.
1910 – Före detta kyparen Alfred Ander rånmördar en kassörska på ett växelkontor i Stockholm. Han döms sedermera till döden och blir den 23 november samma år den siste som avrättas i Sverige. Det dröjer dock till 1921, innan dödsstraffet avskaffas i svensk lag.
1914 – Det amerikanska bilföretaget Ford Motor Company inför 8 timmars arbetsdag och en minimilön på 5 dollar per dag.
1919 – Sedan Berlins polischef Emil Eichhorn har blivit avskedad dagen innan utbryter en tio dagar lång strejk i staden, som får namnet Spartakistupproret (eller Januariupproret).
1924 – Nya tennishallen vid Östermalms idrottsplats i Stockholm invigs.
1925 – Nellie Tayloe Ross tillträder som den första kvinnliga guvernören över en amerikansk delstat (Wyoming), en post hon kommer inneha i nästan två år (till 3 januari 1927).
1930 – Sovjetunionens första kolchos (”kollektivjordbruk”) grundas.
1933 – Golden Gate-bron i amerikanska San Francisco börjar byggas och står klar att invigas 1937.
1943 – Läger börjar inrättas för vissa kategorier av personer, som behöver hållas under uppsikt, bland de 30 000 flyktingar som finns i Sverige.
1945 – Sovjetunionen erkänner den nya sovjetvänliga regimen i Polen.
1946 – Kina erkänner Mongoliska folkrepubliken (Mongoliet) som självständig stat. Det har då gått 25 år sedan Mongoliet 1924 utropade sin självständighet från Kina och det är sovjetledaren Josef Stalin, som har utövat påtryckningar på den kinesiske ledaren Chiang Kai-shek, för att Kina ska erkänna Mongoliets självständighet. I gengäld utverkar Chiang Kai-shek ett löfte av Stalin, att Sovjet inte ska stödja det kinesiska kommunistpartiet.
1949 – Den svenska diplomaten Alva Myrdal blir chef för Förenta nationernas avdelning för sociala frågor i New York.
1957
Den amerikanske presidenten Dwight D. Eisenhower lägger för amerikanska kongressen fram Eisenhowerdoktrinen, som innebär att USA ska ge ekonomiskt stöd åt de länder i Mellanöstern, som vill ha nationellt självbestämmande och militärt stöd åt de länder i området, som hotas av kommunistiska länder.
Enheter ur västtyska försvarsmakten Bundeswehr ställs för första gången under Natobefäl.

1960 – Storflygplatsen Arlanda utanför Stockholm öppnas för trafik, medan invigningen sker först två år senare.
1964
Den kommunistiska tidningen Ny Dags chefredaktör C.-H. Hermansson blir partiledare för Sveriges kommunistiska parti efter Hilding Hagberg.
Påven Paulus VI träffar patriarken Athenagoras I i Jerusalem, vilket blir det första mötet mellan ledarna för romersk-katolska kyrkan och ortodoxa kyrkan sedan 1439. Det leder till att påvens och patriarkens över 900 år gamla ömsesidiga bannlysning av varandra, som har varat sedan 1054, hävs.

1968 – Alexander Dubček efterträder Antonín Novotný som förstesekreterare i Tjeckoslovakiens kommunistiska parti, vilket inleder det tjeckoslovakiska upproret Pragvåren.
1970 – Ett flygplan från bolaget Spantax havererar strax efter starten från Arlanda, varvid fem personer omkommer och fem skadas.
1976 – Det kommunistiska Kambodja byter officiellt namn till Demokratiska Kampuchea, vilket landet heter till 1979.
1980 – USA inför handelssanktioner mot Sovjetunionen på grund av den sovjetiska invasionen av Afghanistan, som inleddes den 24 december året innan. Invasionen leder också till att en rad länder bojkottar de olympiska sommarspelen, som hålls i Moskva detta år.
1982 – Libyska studenter vid Telub i Växjö reser hem i protest sedan de har blivit vägrade utbildning i militära ämnen under den så kallade Telubaffären.
1993
I den amerikanska delstaten Washington blir seriemördaren och sexualbrottslingen Westley Allan Dodd avrättad genom hängning, vilket är senaste gången denna avrättningsmetod använts i USA.
Den Liberiaregistrerade oljetankern MV Braer, lastad med 84 700 ton råolja, går på grund vid Shetlandsöarna i Skottland. Under fyra dygn bryts fartyget i delar och sjunker, varvid oljan läcker ut i havet. Vindriktningen och den storm som bryter sönder oljeklumparna, gör dock katastrofen mildare än man först befarar.


