625 – Sedan Bonifatius V har avlidit två dagar tidigare väljs Honorius I till påve.
939 – Vid Æthelstan den ärorikes död efterträds han som kung av England av sin bror Edmund.
1524 – Franska trupper påbörjar en belägring av Pavia under Italienska krigen.
1810 – USA deklarerar sin annektering av Republic of West Florida, ett område som tidigare varit en spansk koloni.
1808 – Slaget vid Virta bro, Finland, då Johan August Sandels med 2 000 man besegrar en jämnstark rysk styrka.
1922 – Marschen mot Rom. Benito Mussolini och hans fascistiska partikamrater tågar till Rom.
1946 – Frankrike antar en ny konstitution.
1956 – Imre Nagy bildar en nationalistisk regering i Ungern.
1981 – Den sovjetiska ubåten U 137 går på grund utanför Karlskrona.
1991 – Turkmenistan blir självständigt.


