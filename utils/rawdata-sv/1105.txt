1556 – Andra slaget vid Panipat, mellan stormogulen Akbar den store och hans upproriske minister Hemu.
1605 – Krutkonspirationen, då parlamentet (Westminster Palace) skulle sprängas i luften; avslöjandet av konspiratören Guy Fawkes började firas i Storbritannien 1606, kallas Guy Fawkes Night.
1688 – Den engelsk-skotsk-irländske kungen Jakob II:s svärson Vilhelm III landstiger i Torbay i England, vilket inleder den ärorika revolutionen.
1757 – Slaget vid Rossbach under sjuårskriget, där Fredrik den store besegrade en dubbelt så stark fransk-österrikisk armé.
1854 – Slaget vid Inkerman under Krimkriget, där brittiska ställningar anfölls av ryssarna, men dessa kastas tillbaka sedan franska trupper kommit till britternas undsättning.
1885 – Socialisternas marsch publiceras för första gången på svenska i tidningen Social-Demokraten.
1912 – Demokraten Woodrow Wilson vinner presidentvalet i USA.
1937 – I det så kallade Hossbach-protokollet förklarar Adolf Hitler sin avsikt att föra krig.
1949 – Joseph Jackson och Katherine Scruse gifter sig. Deras nio barn (däribland Michael och Janet) blir samtliga framgångsrika artister och musiker. Familjen får senare smeknamnet First family of pop.
1968 – Republikanen Richard Nixon vinner presidentvalet i USA.
1991 – Kiichi Miyazawa blir Japans 78:e premiärminister.
1999 – Den första officiella versionen av det populära spelutvecklarverktyget Game Maker släpps.
2006 – Iraks före detta diktator Saddam Hussein döms till döden genom hängning för brott mot mänskligheten.
2007 – Maffiabossen för Cosa Nostra Salvatore Lo Piccolo och hans son grips.
2007 – Google presenterar mobila operativsystemet Android.


