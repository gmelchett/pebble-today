1492 – Christofer Columbus upptäcker ön Hispaniola.
1590 – Sedan Urban VII har avlidit den 27 september väljs Niccolò Sfondrati till påve och tar namnet Gregorius XIV.
1864 – Den finska tidningen Hufvudstadsbladet grundas i Helsingfors och ges ut för första gången.
1905 – Sir Henry Campbell-Bannerman blir premiärminister i Storbritannien.
1931 – Kristus Frälsarens katedral i Moskva förstörs på order av Josef Stalin.
1969 – Den svenska tv-kanalen TV 2 inleder sina sändningar.
1985 – Thorbjörn Fälldin avgår som partiledare för Centerpartiet.
1995 – Javier Solana utses till ny Nato-chef.
1996 – Madeleine Albright blir USA:s första kvinnliga utrikesminister.
1997 – Den svenska Riksåklagaren begär resning i fallet med mordet på statsminister Olof Palme.
1997 – Åtal väcks mot den så kallade OS-bombaren.
2007 – Tv-programmet Uppdrag granskning avslöjar att en del av den svenska butikskedjan ICA:s butiker missköter hantering av kött, vilket skapar en skandal.


