257 – Sedan Stefan I har avlidit den 2 augusti väljs Sixtus II till påve (denna eller föregående dag).
1422 – Vid Henrik V:s död efterträds han som kung av England och herre över Irland av sin nio månader gamle son Henrik VI.
1888 – Jack Uppskäraren mördar sitt första offer, Mary Nichols.
1897 – I Basel avslutas den av Theodor Herzl initierade Första Sionistiska Kongressen, där World Zionist Organization grundades.
1924 – Paavo Nurmi sätter världsrekord på 10 000 meter.
1931 – Den sista av 4 320 446 tillverkade A-Fordar rullar av bandet.
1928 – Tolvskillingsoperan av Bertolt Brecht och Kurt Weill har urpremiär i Berlin.
1939 – Sandöbrons spann rasar samman.
1957 – Malaya, sedan 1963 udvidgad till Malaysia blir självständigt.
1962 – Trinidad och Tobago blir självständigt.
1987 – Popartisten Michael Jackson släpper musikalbumet Bad.
1994 – Irländska republikanska armén utlyser vapenvila.
1997 – Prinsessan Diana omkommer efter en bilolycka i Paris.
2003 – Kajsa Bergqvist tar VM-brons i höjdhopp i Paris.
2003 – Carl Axel Aurelius och Lennart Koskinen vigs till biskopar av ärkebiskop K.G. Hammar för Göteborgs stift respektive Visby stift.
2006 – Tavlorna Skriet och Madonna som 2004 hade stulits från Munchmuseet i Oslo återfinns av norsk polis.
2008 – Lotta Engberg börjar som programledare i Bingolotto.


