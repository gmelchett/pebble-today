1143 – Alfons VII av Kastilien erkänner konungadömet Portugal som självständigt.
1582 – Den gregorianska kalendern införs.
1665 – Christian-Albrechts-Universität grundas.
1789 – Kvinnotåget till Versailles tvingar med sig Ludvig XVI till Paris under franska revolutionens berömda oktoberdagar.
1793 – Den franska revolutionskalendern införs.
1908 – Bulgarien förklarar sig oberoende från det Osmanska riket.
1910 – Oktoberrevolutionen i Portugal införs.
1962
The Beatles släpper sin första singel Love Me Do.
Första James Bond filmen, Dr. No (Agent 007 med rätt att döda) har premiär.

1967 – Omar Ali Saifuddin III abdikerar och hans son Hassanal Bolkiah blir ny sultan i Brunei.
1969 – Komikergruppen Monty Python gör debut på brittisk tv med serien Monty Pythons flygande cirkus.
1988 – I en folkomröstning om Pinochets framtid vid makten, vinner nejsidan med 56% av rösterna, och därmed återkommer demokratin i Chile.
1997 – Christina Odenberg vigs, som första kvinna i Sverige, till biskop i Lunds stift. Vigningen äger rum i Uppsala domkyrka.
2003
RUT-avdrag föreslås vid en motion i Sveriges riksdag .
Martin Lidberg, Sverige tar VM-guld i 96-kilosklassen i brottning för herrar i Creteil, Frankrike.

2004
Flera av de svenska morgontidningarna ändrar sitt format till tabloid.
Vulkanen Mount Saint Helens blir åter aktiv.

2006 – Fredrik Reinfeldt blir ny statsminister i Sverige efter riksdagsvalet 2006.
2011 – En ny motortrafikled mellan Enånger och Hudiksvall invigs.


