732 – Karl Martell besegrar Abd ar-Rahman ibn Abd Allah i slaget vid Poitiers.
1471 – Slaget vid Brunkeberg (på nuvarande Norrmalm) i Stockholm utkämpas.
1903 – Den brittiska suffragetten Emmeline Pankhurst bildar Women’s Social and Political Union.
1911 – Den republikanska revolutionen i Kina äger rum och kejsardömet störtas. Dagen firas som Taiwans (Republiken Kina) nationaldag.
1930 – Det amerikanska flygbolaget Trans World Airlines bildas.
1935 – Uruppförandet av Porgy och Bess av George Gershwin äger rum i New York.
1964 – Olympiska sommarspelen invigs i Tokyo, Japan. Spelen pågår till den 24:e.
1970 – Öriket Fiji i Stilla havet blir självständigt från Storbritannien. Firas som Fijis nationaldag.
1971 – London Bridge återinvigs i Lake Havasu City, Arizona, USA efter att ha flyttats dit (isärplockad) från London, Storbritannien.
1994 – Musikkanalen VH1 från MTV Networks lanseras i en egen version för Storbritannien och Europa.


