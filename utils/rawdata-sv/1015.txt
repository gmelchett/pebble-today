533 – Belisarius gör sitt formella intåg i Karthago efter att ha erövrat staden från vandalerna.
1582 – Den gregorianska kalendern börjar användas (mest i katolska länder till en början) och ersätter då den julianska.
1552 – Kazankhanatet intas av Ivan den förskräcklige.
1912 – Freden i Ouchy-Lausanne sluts mellan Italien och det osmanska riket, vilket avslutar Tripoliskriget.
1917 – I Paris avrättas den nederländska dansösen Mata Hari som tysk spion.
1935 – Trafikkarusellen Slussen i Stockholm invigs och väcker internationell uppmärksamhet.
1951 – I Love Lucy, en klassisk amerikansk komediserie på tv med Lucille Ball, börjar sändas i USA.
1963 – Frankrike lämnar Bizerte.
1974 – Kulturhuset vid Sergels torg i Stockholm invigs.
1981 – Hårdrocksbandet Metallica bildas.
1990 – Michail Gorbatjov tilldelas Nobels fredspris
1992 – Det svenska Riksidrottsmuseet öppnas.
1997 – TV8 gör sin premiärsändning med fokus på ekonomiprogram i tablån. Några år senare köper MTG den förlusttyngda tv-kanalen.
2003 – Kina sänder för första gången upp en människa i rymden. Yang Liwei färdas 14 varv runt Jorden i rymdskeppet Shenzhou 5.
2007 – Klockan 9.45 släcks de gamla analoga tv-sändningarna från SVT2 och TV4 i marknätet i Skåne och Blekinge.
2011 – Omfattande protester i många städer i världen mot växande ekonomiska klyftor, storföretagens makt och bristande demokrati.


