1276 – Magnus Ladulås gifter sig med Helvig av Holstein.
1417 – Efter att påvestolen har stått tom i över två år väljs Oddone Colonna till påve och tar namnet Martin V. Detta gör slut på den stora schismen, som har varat sedan 1378.
1572 – Tycho Brahe upptäcker en supernova i stjärnbilden Cassiopeia.
1868 – Den nederländska diplomaten Dirk de Graeff van Polsbroek sluter det första handelsfördraget mellan Sverige-Norge och Japan.
1889 – Washington blir den 42:a delstaten att ingå i den amerikanska unionen.
1918 – Ett vapenstilleståndsavtal undertecknas klockan 05:10 och träder i kraft kl. 11:00, vilket markerar slutet på striderna i det första världskriget.
1918 – Polen blir självständigt från Sovjetryssland.
1923 – Adolf Hitler arresteras efter den så kallade ölkällarkuppen.
1965 – Rhodesia förklarar sig självständigt från Storbritannien, vilket resulterar i att FN, på Storbritanniens inrådan, inleder sanktioner mot landet.
1975 – Angola deklarerar sin självständighet från Portugal.
1978 – Maumoon Abdul Gayoom besegrar Ibrahim Nasir och blir Maldivernas president.
2000 – En brand bryter ut i ett bergbanetåg i Kaprun, Österrike varvid 155 människor omkommer.


