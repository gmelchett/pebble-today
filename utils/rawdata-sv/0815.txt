1057 – När Macbeth stupar i strid efterträds han som kung av Skottland av sin styvson Lulach. Denne stupar dock själv ett halvår senare i strid mot Malkolm III.
1843 – Nöjesfältet Tivoli öppnar i Köpenhamn.
1944 – Allierad landstigning i södra Frankrike – Operation Dragoon.
1945 – Japan kapitulerar.
1946 – Muonionalusta II meteoriten hittas, ett av få upptäckta meteoritnedslag i Sverige
1947 – Brittiska Indien delas i Indien och Pakistan och båda dessa stater uppnår självständighet från Storbritannien efter åratal av icke-våldsprotester. I Indien blir Jawaharlal Nehru landets förste premiärminister.
1950 – Jordbävning och översvämning drabbar Assam i Indien och förorsakar 574 dödsoffer och gör cirka 5 miljoner människor hemlösa.
1960 – Nuvarande Kongo-Brazzaville blir självständigt från Frankrike.
1965 – Svåra rasupplopp i stadsdelen Watts i Los Angeles, Kalifornien, USA; 20 000 nationalgardister kallas in för att kontrollera upploppen, som kräver 28 dödsoffer och 676 skadade.
1969 – Den legendariska Woodstockfestivalen inleds i norra delen av staten New York, USA och pågår till den 17:e.
1971 – Storbritanniens protektorat över Bahrain upphör.
1982 – Ledningen för SVT beslutar avstänga Åsa Bodén fram till valet, eftersom hon uttalat sig om löntagarfonderna.
1987 – Bestraffning med rotting förbjuds officiellt i brittiska skolor, med undantag för friskolorna.
1988 – En bussolycka inträffar i Norge med barn och föräldrar ifrån Kvarnbackaskolan i Kista. 16 människor dör, varav 12 var barn, och 19 skadas.
1991 – Paul Simon ger en konsert i Central Park i New York inför cirka 750 000 åskådare.
1996 – Belgisk polis griper pedofilen Marc Dutroux och genomför en razzia i hans hem.
1998 – Verkliga IRA genomför ett bombattentat i Omagh, Nordirland som dödar 29 personer.


