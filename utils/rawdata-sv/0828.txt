
1649 – Fartyget Kattan, som är på väg med förnödenheter till kolonin Nya Sverige, förliser vid Puerto Rico.
1907 – Transportföretaget United Parcel Service grundas i Seattle, Washington.
1910 – Kungariket Montenegro utropas.
1942 – Gunder Hägg sätter sitt sjätte världsrekord för säsongen genom att springa 3000 meter på 8.01,2.
1963 – Martin Luther King håller sitt berömda tal I Have a Dream i samband med marschen till Washington för arbete och frihet.
1996 – Skilsmässa mellan prins Charles av Storbritannien och prinsessan Diana.
2002 – Tillverkningen av Chevrolet Camaro upphör.
2003 – Patrik Kristiansson tar VM-brons i stavhopp i friidrotts-VM.
2006 – Iron Maiden släpper det nya studioalbumet A Matter Of Life And Death med tillhörande turné.
2010 – Botniabanan invigs officiellt.


