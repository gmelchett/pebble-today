1252 – Birger jarl och ärkebiskop Jarler fäster sina sigill under ett brev, utfärdat i Stockholm. Detta, tillsammans med det brev som har utfärdats på platsen tidigare under sommaren gör att detta år brukar räknas som Stockholms födelseår och traditionellt har även Birger jarl därav räknats som dess grundare.
1458 – Sedan Calixtus III har avlidit den 6 augusti väljs Enea Silvio Piccolomini till påve och tar namnet Pius II.
1719 – Hela staden Norrtälje bränns ner av ryssarna under stora nordiska kriget.
1772 – Gustav III genomför en oblodig statskupp för att återbörda makten från särintressen och utländska makter, se Gustav III:s statskupp. Kallas även "gustavianska revolutionen".
1809 – Ryssarna besegrar svenskarna i slaget vid Sävar under finska kriget.
1922 – Drottningholms slottsteater återinvigs.
1989 – Den Paneuropeiska picknicken, som var första steget i järnridåns fall, hålls i Sopron, Ungern.
1991 – Ett misslyckat kuppförsök genomförs mot Michail Gorbatjov av konservativa krafter inom det sovjetiska kommunistpartiet och försvarsmakten. Gorbatjov sätts i husarrest i sin villa vid Svarta havet. Efter starkt motstånd mot kuppmakarna från allmänheten samt Boris Jeltsin, står kuppens misslyckande klart den 22 augusti och Gorbatjov kan återvända som president.
2009 – Första National Domestic Workers Day i Uruguay firas..


