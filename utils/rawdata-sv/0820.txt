1922 – Första ordinarie Damolympiaden hålls, 5 nationer tävlar i Paris
1953 – Sovjetunionen spränger sin första vätebomb.
1959 – Första lyftet av regalskeppet Vasa.
1960 – Republiken Senegal lämnar Malifederationen.
1989 – Bröderna Lyle och Erik Menendez skjuter ihjäl sina föräldrar Jose och Kitty Menendez i familjens villa på Elm Drive i Beverly Hills, Los Angeles, Kalifornien, USA.
1991 – Estland förklarar sig självständigt från Sovjetunionen.


