1058 – Sedan Stefan IX har avlidit den 29 mars väljs Gérard av Burgund till påve och tar namnet Nicolaus II.
1790 – Förenta staternas kongress flyttar från New York till Philadelphia.
1877 – Thomas Alva Edison läser in barnramsan Mary had a Little Lamb på sin fonograf och detta blir därmed historiens första inspelning av en mänsklig röst.
1884 – Washingtonmonumentet färdigt.
1906 – Oscarsteatern i Stockholm invigs.
1907 – Vid en kolgruveexplosion i Monongah, West Virginia omkommer 361 gruvarbetare.
1917
Finland förklarar sig självständigt.
I hamnen i Halifax i Nova Scotia i Kanada kolliderar två fartyg – det ena är det franska Mont Blanc som är lastat med ammunition – och förorsakar en explosion, som dödar mer än 2 000 personer och raserar en stor del av staden.

1921 – Irland utropas som självständigt från Förenade kungariket, men ingår fortfarande i Brittiska imperiet.
1926 – Fotbollsklubben Trelleborgs FF bildas.
1941 – Röda armén under Zjukov går till motanfall mot de av kylan paralyserade tyskarna framför Moskva.
1957 – Första amerikanska försöket att skjuta upp en satellit misslyckas när raketen exploderar under start.
1973 – Gerald Ford tillträder som vicepresident i USA och ersätter Spiro Agnew, som avgår på grund av skattefiffel.
1984 – Leo Honkala får sparken som ordförande i Svenska Brottningsförbundet.
1989 – École Polytechnique-massakern: en psykiskt sjuk man dödar 14 unga kvinnor i Montréal.
1992
Schweiz säger i folkomröstning nej till EES-avtalet.
Sex ungdomar skottskadas av en 26-årig man i Mora. En av dem avlider senare.
Babri Masjid rivs av hinduer.

1998 – Gävle drabbas av ett våldsamt snöoväder. Skolor stängs och invånarna uppmanas att stanna inomhus.
2003 – Statsminister Göran Persson och Systembolagets vd Anitra Steen gifter sig på Harpsund.
2007 – Dokumentären I takt med tiden avslöjar en köttskandal på Ica.


