498 – Sedan Anastasius II har avlidit tre dagar tidigare väljs Symmachus till påve i Lateranen, medan Laurentius väljs till motpåve i Santa Maria Maggiore.
1286 – Den danske kungen Erik Klipping blir mördad i Finderup av en grupp sammansvurna adelsmän. Erik efterträds dock omgående som kung av Danmark av sin son Erik Menved och redan nästa år är de sammansvurna infångade och avrättade.
1357 – En uppgörelse nås mellan den svenske kungen Magnus Eriksson och hans son Erik Magnusson i Stockholm i och med Novemberuppgörelsen. I och med denna tillfaller även Södermanland, Västmanland, Dalarna, större delen av Uppland samt Stockholms slott Erik.
1955 – Sovjetunionen genomför sin första vätebombsprovsprängning i Kazachstan.
1956 – Imre Nagy fängslas och bortförs av sovjetiska trupper.
1963 – USA:s president John F. Kennedy skjuts till döds när han färdas i en öppen bilkolonn längs Dealey Plaza i Dallas, Texas, USA.
1995 – Animerade långfilmen Toy Story från Disney Pixar har världspremiär i USA.
2005 – Angela Merkel blir Tysklands första kvinnliga förbundskansler.
2010 – My Chemical Romances fjärde album "Danger Days: The True Lifes Of The Fabulous Killsjoys" släpps.


