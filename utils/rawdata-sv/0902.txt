44 f.Kr. – Kleopatra VII av Egypten utropar Caesarion (även känd som Ptolemaios XV Caesar) till sin medregent.
31 f.Kr. – Octavianus besegrar sin rival Antonius i slaget vid Actium, och kan därmed etablera sig som ensam härskare över Romarriket.
1666 – Stora branden i London startar och härjar till den 5 september
1675 – Danmark förklarar Sverige krig vilket blir inledningen till skånska kriget
1854 – Bomarsunds fästning på Åland sprängs av de engelsk-franska erövrarna.
1870 – Tyskland besegrar fransmännen vid Sedan.
1945 – Japans officiella kapitulation äger rum ombord på USS Missouri, sedan japanerna har accepterat ovillkorlig kapitulation den 14 augusti. Närvarande är general Douglas MacArthur och amiral Chester Nimitz för USA samt utrikesminister Mamoru Shigemitsu och Yoshihiro Umetsu för Japan.
1967 – Sträckan Östermalmstorg–Ropsten öppnas i Stockholms tunnelbana.
1990 – Transnistrien förklarar sig självständigt från Moldavien.
2001 – Medelhavsspelen 2001 arrangeras i Tunis, Tunisien.
2003 – Sverige tar brons vid EM i modern femkamp i Tjeckien.


