

=== Före 1900-talet ===
38 f.Kr. – Den blivande romerske kejsaren Octavianus gifter sig med Livia Drusilla, som har två söner från sitt förra äktenskap. De förblir sedan gifta i 52 år, utan att få några egna barn, varför Octavianus vid sin död efterträds som romersk kejsare av Livia Drusillas son Tiberius.
1287 – Den aragoniske kungen Alfons III låter invadera och erövra medelhavsön Menorca.
1377 – Påven Gregorius XI flyttar tillbaka påvestolen från Avignon till Rom. Detta avslutar ”påvarnas babyloniska fångenskap”, som inleddes 1309, när den franske kungen Filip IV lät flytta påvestolen till Avignon.
1437 – Erik Pukes upprorshär besegrar marsken Karl Knutssons yrkesarmé i slaget vid Hällaskogen. Trots detta blir Erik Puke vid stilleståndsförhandlingarna i Skultuna den 22 januari infångad och avrättas i början av februari.
1482 – Ett stillestånd sluts mellan Sverige och Ryssland i Novgorod. Detta avslutar det krig som har varat mellan länderna sedan 1479 och medför inga större förändringar.
1524 – Den italienske upptäcktsresanden Giovanni da Verrazano påbörjar en resa till Nordamerika för att, för fransk räkning, hitta en passage till Kina.
1562 – Den franska regenten Katarina av Medici låter utfärda ediktet i Saint-Germain, som ger de franska hugenotterna (protestanterna) begränsad religionsfrihet.
1595 – Den franske kungen Henrik IV förklarar krig mot Spanien, eftersom den franska katolska ligan stöds av detta land under det pågående franska inbördeskriget.
1605 – Miguel Cervantes publicerar första delen av romanen Don Quijote. Den andra delen ges ut år 1615.
1648 – Det engelska långa parlamentet antar lagförslaget Vote of No Address, som avbryter parlamentets förhandlingar med kungen Karl I och inleder den andra fasen av det engelska inbördeskriget. Drygt ett år senare är kungen tillfångatagen och avrättas.
1773 – Den brittiske kaptenen James Cook och hans besättning blir de första européerna som korsar den södra polcirkeln.
1781 – Amerikanska trupper ledda av brigadgeneral Daniel Morgan besegrar britterna under överstelöjtnant Banastre Tarleton i striden om Cowpens i South Carolina under det pågående amerikanska revolutionskriget.
1852 – Storbritannien erkänner boerkolonierna i Transvaal som självständiga, sedan de har utropat sin självständighet 1845.
1873 – Den amerikanska armén blir besegrad av en grupp modocindianer i första slaget om befästningen under det så kallade Modockriget.
1883 – Det svenska företaget Elektriska Aktiebolaget i Stockholm grundas. Redan 1890 byter det namn till Allmänna Svenska Elektriska Aktiebolaget (Asea) och 1987 går det samman med schweiziska Brown Boveri och bildar då ABB.
1885 – Brittiska trupper besegrar en stor dervisk armé i slaget vid Abu Klea i Sudan under det pågående mahdistupproret.
1893 – Den hawaiianska ligan, som består av amerikaner och européer bosatta på Hawaii och leds av Lorrin A. Thurston, störtar det hawaiianska kungadömet genom att avsätta örikets sista drottning Lili'uokalani. Ligans mål är att USA ska annektera Hawaii, vilket sker 1898, men det dröjer till 1959, innan Hawaii blir en amerikansk delstat.
1899 – USA annekterar de obebodda Wakeöarna i Stilla havet.


=== 1900–1949 ===
1904 – Den ryske pjäsförfattaren Anton Tjechovs skådespel Körsbärsträdgården har urpremiär på Konstnärliga teatern i Moskva.
1909 – Den svenske upptäcktsresanden Sven Hedin återvänder från sin senaste Asienresa till Sverige och tas emot med stor pompa och ståt. Förutom en liten resa till östra Medelhavet och Irak 1916 dröjer det till slutet av 1920-talet, innan han ger sig ut på nya expeditionsresor och han blandar sig nu istället i den svenska politiken och särskilt den uppblossande försvarsfrågan.
1912 – Den brittiske polarforskaren Robert Scotts expedition når sydpolen under kapplöpningen att ta sig dit först. Den norska polarexpeditionen under Roald Amundsen har dock lyckats ta sig dit redan en månad tidigare, så Scotts expedition har alltså förlorat kapplöpningen. Under återfärden till Antarktis kust omkommer expeditionsmedlemmarna i en snöstorm och deras kroppar återfinns av en annan expedition i november samma år.
1913 – Raymond Poincaré vinner det franska presidentvalet och tillträder posten som landets president 18 februari.
1916
Galtströms järnbruk i Medelpad levererar sitt sista lass järnmalm, varefter bruket läggs ner efter att ha funnits sedan 1673. Den intilliggande sågen förblir dock öppen i ytterligare tre år.
Golfintressegruppen Professional Golfers' Association (PGA), som har bildats i London 1901, får en amerikansk systerorganisation i Professional Golfers' Association of America, som grundas denna dag. Den svenska avdelningen grundas 1932.

1917 – USA köper den danska kolonin Jungfruöarna för 25 miljoner dollar. Den 31 mars samma år tar USA ögruppen i besittning och byter då namn på den till Amerikanska Jungfruöarna, för att skilja dem från Brittiska Jungfruöarna, som ligger strax intill.
1924
Det svenska kommunalarbetarförbundet förklarar att de poliser, som har skickats från Stockholm till Sundsvall med anledning av den pågående renhållningsarbetarstrejken där, måste återsändas.
Ett utbrott av mul- och klövsjuka konstateras bland bondgårdsdjur i Skåne. Mycket snart beslutas att man ska börja nödslakta smittade djur, för att få bukt med sjukdomen.

1929 – Den amerikanske serietecknaren E.C. Segar publicerar den första strippen om sjömannen Karl-Alfred (engelska Popeye), som blir stark av spenat, i dagspresserien Thimble Theatre (Fingerborgsteatern). Till en början är Karl-Alfred där en bifigur, men redan två år senare har han blivit så framgångsrik, att serierna om honom publiceras över hela USA.
1938 – 20 000 arbetare inom den svenska hotell- och restaurangbranschen lockoutas och så gott som alla hotell och restauranger i Sverige är stängda i två månader, innan konflikten är löst.
1944 – Sveriges utrikesminister Christian Günther meddelar att allt kulturellt samarbete mellan Sverige och Tyskland nu har upphört. När det pågående andra världskriget vid det här laget har börjat gå märkbart sämre för tyskarna gör Sverige större och större avkall på den undfallande politiken mot Tyskland.
1945
Sovjetiska trupper erövrar den nästan helt raserade polska huvudstaden Warszawa från tyskarna.
Tyskarna påbörjar evakueringen av koncentrationslägret Auschwitz då sovjetiska trupper närmar sig. Den 27 januari befrias lägret av det sovjetiska trupperna.
Den svenske diplomaten Raoul Wallenberg, som under andra världskriget har räddat ett stort antal ungerska judar, arresteras av sovjetiska trupper i Budapest. Hans vidare öden är höljda i dunkel och det är osäkert om den officiella sovjetiska förklaringen, att han dör i Lubjankafängelset 1947, verkligen stämmer. Under kommande årtionden ställer svenska myndigheter då och då krav på Sovjet att få veta vad som har hänt Wallenberg, men utan resultat.

1946 – Förenta nationernas säkerhetsråd håller sitt första möte i Church House i London.
1949 – Den första amerikanska tv-situationskomedin The Goldbergs har premiär i amerikansk tv.


=== 1950–1974 ===
1950 – Ett rån utförs mot det amerikanska säkerhetsföretaget The Brink's Company i Boston, när 11 män stjäl över två miljoner dollar från en av företagets pansarbilar.
1961 – USA:s president Dwight D. Eisenhower tar avsked av det amerikanska folket genom en tv-utsändning tre dagar innan han lämnar presidentposten. I sitt tal varnar han för makten som har samlats hos militärindustrikomplexet.
1966 – Ett B-52-bombflygplan kolliderar med ett plan av typen KC-135 Stratotanker över Spanien och tappar fyra 70-kilotonsvätebomber nära staden Palomares och en femte i havet utanför kusten.
1969 – Det brittiska rockbandet Led Zeppelin ger ut sitt debutalbum med samma namn.
1973 – Ferdinand Marcos, som har varit Filippinernas president sedan 1965, utnämns till landets president på livstid. Missnöje med hans styre och sätt att sköta landets ekonomi leder dock till att han blir avsatt 1986.


=== 1975–1999 ===
1977 – Den amerikanske mördaren Gary Gilmore avrättas genom arkebusering i Utah och blir den förste i USA som avrättas efter att högsta domstolen året före har tillåtit delstaterna att återinföra dödsstraffet.
1984 – En FN-konferens om förtroendeskapande åtgärder och nedrustning öppnas i Stockholm av Sveriges utrikesminister Lennart Bodström.
1991
Amerikanska flygstridskrafter påbörjar Operation Ökenstorm, genom att anfalla Irak och därmed inleda kuwaitkriget, sedan Irak har ignorerat Förenta nationernas uppsatta krav att senast den 15 januari retirera från det ockuperade Kuwait. Det irakiska flygvapnet slås snabbt ut av anfallet.
Vid Olav V:s död efterträds han som kung av Norge av sin son Harald V.
Den isländska vulkanen Hekla får ett utbrott, som varar fram till 11 mars.

1994
Den svenska radiokanalen Megapol (sedermera Mix Megapol), som ägs av Bonnierkoncernen, inleder sina sändningar, till en början över Stockholm och Mälardalen.
En jordbävning inträffar i Los Angeles-förorten Northridge, med en magnitud på 6,7 på richterskalan. 57 människor omkommer och materiella skador till ett värde av cirka 12 miljarder dollar uppstår. Detta är därmed den mest kostsamma jordbävningen i USA:s historia.

1995 – En jordbävning inträffar i den japanska staden Kobe, med en magnitud på 7,3 på richterskalan. 6 434 personer omkommer och 212 443 blir hemlösa.
1996 – Tjeckien ansöker om medlemskap i Europeiska unionen (EU), vilket man uppnår 2004.
1998 – På webbplatsen The Drudge Report avslöjar Matt Drudge att USA:s president Bill Clinton har haft sexuellt umgänge med vita huset-praktikanten Monica Lewinsky, vilket blir inledningen på Lewinsky-affären.


=== 2000–2009 ===
2002 – Vulkanen Nyiragongo i Kongo-Kinshasa får ett utbrott, vilket tvingar uppskattningsvis 400 000 människor att fly.
2005
Det svenska elektronikföretaget Sibas vd Fabian Bengtsson blir kidnappad i centrala Göteborg. Han återfinns välbehållen i Slottsskogen den 3 februari.
En mosaik från 100-talet upptäcks på Colle Oppio i Rom.

2007 – Den så kallade Domedagsklockan, som anger hur nära världen är ett kärnvapenkrig (där midnatt anger att ett sådant har utbrutit) sätts till fem minuter före midnatt, efter att Nordkorea har inlett kärnvapentester. Den 14 januari 2010 flyttas den en minut bakåt till sex minuter före midnatt.


=== 2010– ===
2010 – På ett 30-tal platser runt om i världen genomförs manifestationer i protest mot det amerikanska bilföretaget General Motors planer på att lägga ner biltillverkaren Saab. Företaget blir kvar i ytterligare nästan två år, men den 19 december 2011 begärs Saab i konkurs.


