479 f.Kr. – Genom det avgörande slaget vid Plataiai i Boiotien avbryts den persiska invasionen av Grekland, när den persiske generalen Mardonios besegras av grekerna under den förre spartanske kungen Leonidas I:s brorson Pausanias befäl. Den atenska arméavdelningen leds av den återtagne Aristides. Mardonios stupar under slaget och grekerna tar mycket byte. Thebe erövras strax därefter, varvid Pausanias låter avrätta stadens kollaboratörer.
479 f.Kr. – Perserna besegras till sjöss av en grekisk flotta under Leotychidas från Sparta och Xanthippos från Aten i slaget vid Mykale utanför Lydiens kust vid Mindre Asien.
55 f.Kr. – Julius Caesar landstiger i England för första gången.
1634 – Slaget vid Nördlingen.
1689 – Kina och Ryssland sluter fördraget i Nertjinsk.
1772 – Tortyren avskaffas i Sverige, genom ett kungligt brev av Gustav III.
1828 – Brasilien erkänner Uruguay genom fördraget i Montevideo.
1859 – Världens första oljekälla borras i Titusville, Pennsylvania, USA av Edwin Drake.
1883 – Ön Krakatau får fyra våldsamma vulkanutbrott och kraftiga tsunamier.
1896 – Världens kortaste krig utkämpas mellan Zanzibar och Storbritannien, som avgår med segern efter 38 minuter.
1922 – Folkomröstningen om rusdrycksförbud i Sverige genomförs.
1926 – Internationella kvinnospelen 1926 (den andra Damolympiaden) inleds på Slottsskogsvallen i Göteborg
1979 – Irländska Republikanska Armén genomför sitt mest lyckade överfall på brittiska armén då 18 soldater dör i grevskapet Down.
1991 – Sverige och EU erkänner de baltiska staternas självständighet.


