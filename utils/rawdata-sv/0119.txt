973 – Sedan Johannes XIII har avlidit året innan väljs Benedictus VI till påve.
1419 – Under det pågående hundraårskriget intar engelsmännen, under kung Henrik V:s ledning, den franska staden Rouen och därmed faller hela Normandie i engelsmännens händer.
1520 – Under det pågående kriget mellan Sverige och Danmark blir svenskarna besegrade av danskarna i slaget på Åsundens is vid Bogesund (nuvarande Ulricehamn). Den svenske riksföreståndaren Sten Sture den yngre såras dödligt i slaget och avlider den 3 februari på väg mot Stockholm.
1909 – Den svenske träsnidaren Axel Petersson, som blir känd som Döderhultarn, får sitt genombrott på en utställning för svenska humorister.
1913 – Eftersom det rådande modet förespråkar, att damer i sina hattar ska ha så kallade ”hattnålar” (spetsiga stavar, som är flera decimeter långa), börjar man på Stockholms spårvägar sälja hattnålsskydd, för att skydda andra passagerare mot dessa, som man kallar ”fruktansvärda spjut”.
1915 – Under det pågående första världskriget släpper en tysk zeppelinare bomber över Storbritanniens huvudstad London, vilket blir första gången som mer än 20 personer dödas i en bombräd.
1925 – Den svenske företagaren Albin Hagström registrerar sin firma med samma namn, som inriktar sig på försäljning av dragspel och så småningom blir internationellt känd.
1942 – Under det pågående andra världskriget inleder Japan en invasion av den brittiska asiatiska kolonin Burma. Snart har japanerna trängt ner till Filippinerna och hotar Australien.
1943 – Det svenska minfartyget Älvsnabben sjösätts. Fram till dess att hon skrotas 1982 kommer hon att göra 25 långresor.
1960 – Det svenska flygplanet Orm Viking havererar vid inflygningen till Turkiets huvudstad Ankara, varvid 42 personer (därav 6 svenskar) omkommer i flygbolaget SAS' första haveri.
1981 – USA och Iran kommer, genom fördraget i Alger, överens om att de 52 amerikaner, som har suttit som gisslan på amerikanska ambassaden i Teheran sedan 4 november 1979, ska friges. Frigivningen sker dagen därpå, några minuter efter att Ronald Reagan har tillträtt posten som USA:s president.
1987 – Det svenska bioteknikföretaget Fermenta avstängs från börsen sedan revisorerna har funnit allvarliga fel i dess bokföring.
1997 – Den palestinska befrielseorganisationen PLO:s ledare Yassir Arafat återvänder till Hebron på Västbanken efter mer än 30 års bortavaro.


