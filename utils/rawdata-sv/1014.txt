222 – Sedan Calixtus I har avlidit tidigare samma år väljs Urban I till påve.
1066 – I slaget vid Hastings besegrar en normandisk här under Vilhelm Erövraren en engelsk här under den anglosaxiske kungen Harald Godwinson. Sedan denne har stupat i slaget utropas hans brors sonson Edgar till kung av England. Detta slag, och då även året, brukar betraktas som det definitiva slutet på vikingatiden i de nordiska länderna.
1435 – Den avsatte Erik av Pommern erkänns åter som svensk kung.
1648 – I staden Münster sluts den westfaliska freden, vilket avslutar det trettioåriga kriget.
1758 – Slaget vid Hochkirch under sjuåriga kriget.
1758 – Norrköpings Tidningar kommer ut med sitt första nummer.
1806 – Dubbelslaget vid Jena/Auerstedt under napoleonkrigen.
1900 – Sigmund Freud publicerar boken Drömtydning.
1939 – Det brittiska slagskeppet Royal Oak sänks i Scapa Flow av en tysk ubåt.
1943 – Filippinerna förklarar sig självständigt.
1944 – Generalfältmarskalk Erwin Rommel begår självmord på Hitlers order.
1958 – Autonoma Republiken Malagasy utropas.
1947 – Chuck Yeager, amerikansk testpilot, flyger som förste man fortare än ljudets hastighet i planflykt med Bell X-1.
1961 – Radioprogrammet Tio i topp sänds för första gången.
1962 – Kubakrisen börjar med att ett U2-plan fotograferar sovjetiska kärnvapenbärande robotar på Kuba.
1964 – Nikita Chrusjtjov avsätts som Sovjetunionens ledare.
2005 – Pontonkranen Lodbrok kolliderar med Essingebron på Essingeleden, se kollisionen mellan Lodbrok och Essingebron.
2006 – Maria Borelius avgår som handelsminister efter en rekordkort tid.
2009 – Räkmackans dag instiftas på Arlanda.
2010 – De 33 gruvarbetare, som har suttit instängda 600 meter under marken i den chilenska San José-gruvan sedan gruvan rasade den 5 augusti blir alla räddade, genom att hissas upp med hjälp av en kapsel genom ett specialborrat hål.
2012 – Felix Baumgartner slår rekord genom att hoppa från stratosfären och göra längsta fritt fall, på en höjd av 39 068 meter.


