1547 – Ivan IV (sedermera känd som Ivan den förskräcklige), som har varit furste av Moskva sedan 1533, blir den förste ryske härskaren som antar titeln tsar av Ryssland.
1900 – Det svenska politiska partiet Liberala samlingspartiet (föregångare till Folkpartiet) bildas och har som mål att införa allmän rösträtt i Sverige. 1924 splittras partiet i två delar, eftersom man inte kan enas i frågan om ett svenskt spritförbud.
1909 – Den brittiske polarforskaren Ernest Shackletons expedition finner den magnetiska sydpolen.
1924 – En polisstyrka på 20 man sänds från Stockholm till Sundsvall med anledning av den strejk, som pågår bland renhållningsarbetarna där.
1936 – Den svenska stiftelsen Solstickan bildas. Överskottet på försäljning av de tändsticksaskar, som har en etikett ritad av Einar Nerman, ska gå till hjälp åt kroniskt sjuka barn och gamla.
1940 – De första av de barn, som evakueras från det krigsdrabbade Finland (som blir kända som finländska krigsbarn), anländer till Sverige vid Haparanda.
1949 – Den svenske fotbollsspelaren Gunnar Nordahl skriver kontrakt med den italienska fotbollsklubben AC Milan och blir därmed Sveriges förste professionelle fotbollsspelare.
1960 – Flygplanet Saab 35 Draken blir det första svenska flygplanet, som överskrider dubbla ljudhastigheten.
1972 – Borrning efter olja inleds på Gotland. Projektet blir dock ett misslyckande, eftersom man inte hittar några nämnvärda mängder, och läggs därför snart ner.
1992
Sverige erkänner staterna Armenien, Azerbajdzjan, Kazakstan, Kirgizistan, Moldavien, Tadzjikistan, Turkmenistan och Uzbekistan, som har förklarat sig självständiga från det sönderfallande Sovjetunionen, samt Kroatien och Slovenien, som har förklarat sig självständiga från Jugoslavien. Erkännandet av de jugoslaviska staterna sker dagen efter att Europeiska gemenskaperna (EG) har erkänt dem.
Lotteri-tv-programmet Bingolotto får dispens att börja sändas nationellt i Sverige, trots att ett sådant rikstäckande lotteri enligt gällande svensk lotterilag inte är tillåtet.

2003 – Rymdfärjan Columbia påbörjar sitt 28:e uppdrag, vilket blir dess sista, eftersom den havererar 16 dagar senare.
2009 – Ett flygplan tvingas nödlanda på Hudsonfloden mitt i New York efter att en flock fåglar har flugit in i planets motorer. Samtliga ombordvarande överlever landningen.


