1872 – Skeppet Mary Celeste avseglar med en last av råsprit; det försvinner och återfinns en tid senare (5 december 1872) övergivet under mystiska omständigheter.
1905 – Christian Lundeberg avgår som svensk statsminister och efterträds av Karl Staaff.
1916 – Woodrow Wilson blir omvald till president i USA.
1917 – Oktoberrevolutionen i Ryssland: Bolsjevikerna stormar Vinterpalatset. Denna dag firades från 1918 i Sovjetunionen som Den stora socialistiska oktoberrevolutionsdagen.
1931 – Den svenska ungdomsorganisationen Unga Örnar bildas.
1940 – Bron Tacoma Narrows Bridge, utanför staden Tacoma, i nordvästra USA börjar svänga fram och tillbaka, tills den kollapsar.
1944 – Franklin D. Roosevelt väljs för fjärde gången till USA:s president.
1956 – För att avbryta Suezkrisen antar FN:s generalförsamling en resolution som kräver att Storbritannien, Frankrike och Israel omedelbart återkallar sina trupper från Egypten.
1972 – Richard Nixon blir omvald till president i USA, en presidentperiod som slutar med hans förtidiga avgång.
1990 – Mary Robinson blir omvald till president i Irland.
1996 – NASA skjuter upp Mars Global Surveyor från Cape Canaveral AFS.
2000 – George W. Bush väljs till president i USA i ett mycket jämnt och omdiskuterat val.
2001 – Belgiens statliga flygbolag Sabena går i konkurs.
2004 – Slaget om Falluja börjar i Irak under USA:s operation Phantom Fury under Irakkriget.
2007 – Massakern på Jokelaskolan i Tusby, Finland.
2012 - Jordbävningen i Guatemala.


