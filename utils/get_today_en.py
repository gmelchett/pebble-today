#!/usr/bin/env python3

import wikipedia

print("Downloading events of the day - English")

months = ("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December")
lengths = (31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31)

for i in range(len(months)):
    for day in range(1, lengths[i]+1):
        t = "%s %d" % (months[i], day)
        print(t)
        start = False
        e = ""
        for l in wikipedia.page(t).content.split("\n"):
            if start and l == '== Births ==':
                break
            if start:
                e += l + "\n"
            if l == '== Events ==':
                start = True
        open("rawdata-en/%02d%02d.txt" % (i+1, day), "w").write(e)
print("Done -- Lot of dates will require manual editing before the compress script can be used.")





