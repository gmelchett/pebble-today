#!/usr/bin/env python3

import zlib, sys, os, json, datetime, argparse, random

month_lengths = (31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31)


def load_data(language):
    data = {}
    for i in range(len(month_lengths)):
        for day in range(1, month_lengths[i]+1):
            try:
                data["%02d%02d" % (i+1, day)] = open("rawdata-%s/%02d%02d.txt" % (language, i+1, day)).read()
            except:
                print("Failed to read: rawdata-%s/%02d%02d.txt" % (language, i+1, day))
                sys.exit(1)

    return data

def only_events(data, args):

    curr_year = None
    total_size = 0
    longest_line = 0

    data2 = {}

    for i in range(len(month_lengths)):
        for day in range(1, month_lengths[i]+1):
            prev_year = None
            d2 = ""
            atleast_one = False

            day_events = {}

            for l in data["%02d%02d" % (i+1, day)].split("\n"):
                if not len(l):
                    continue
                if l.startswith("==="):
                    continue

                if l.startswith("AD "):
                    l = l[3:]

                if not l.isdigit():
                    if l.split(" ")[0].isdigit():
                        if " – " in l:
                            m = l.split("–")
                        elif " - " in l:
                            m = l.split("-")
                        else:
                            print("%02d%02d.txt" % (i+1, day))
                            print(l)
                            raise
                        year = m[0].strip()
                        if year.endswith(" f.Kr."):
                            year = - int(year[:-6])
                        elif year.endswith(" BC"):
                            year = - int(year[:-3])
                        elif year.endswith(" BCE"):
                            year = - int(year[:-4])
                        elif year.endswith(" AD"):
                            year = int(year[:-3])
                        else:
                            year = int(year)
                        event = l[len(m[0])+2:].strip()
                    else:
                        year = curr_year
                        event = l
                else:
                    curr_year = int(l)
                    continue

                total_size += len(event.encode())

                if longest_line < len(event.encode()) + 10: # Room for BC etc.
                    longest_line = len(event.encode()) + 10

                if len(args.exclude) and args.exclude in event:
                    continue

                event = event.replace(". ", ".").replace(", ", ",").strip(".")

                if year in day_events:
                    day_events[year].append(event)
                else:
                    day_events[year] = [event]

            today = ""
            for y in sorted(day_events):
                if y < args.begin:
                    continue
                if y > args.end:
                    break

                for e in day_events[y]:
                    today += "%d %s\n" % (y, e)
                    if args.only_one:
                        break

            if args.atleast_one and not len(today):
                r = random.randint(0, len(day_events)-1)
                k = 0
                for y in day_events:
                    if k == r:
                        today += "%d %s\n" % (y, day_events[y])
                        break
                    k += 1
            data2["%02d%02d" % (i+1, day)] = today

    return data2, total_size, longest_line


def count_words(data, words, w_len):

    for i in range(len(month_lengths)):
        for day in range(1, month_lengths[i]+1):
            for l in data["%02d%02d" % (i+1, day)].split("\n"):
                if not len(l):
                    continue
                for j in range(0, len(l) - w_len):
                    w = l[j:j+w_len]
                    if not w in words:
                        words[w] = w_len
                    else:
                        words[w] += w_len
    return words

def analyze_words(data):
    words = {}            
    for w in range(5, 20):
        words = count_words(data, words, w)

    return words

def compress(data, w, number):
    count = 0
    saved = 0
    for i in range(len(month_lengths)):
        for day in range(1, month_lengths[i]+1):
            if number < 10:
                c = "#"
            elif number < 100:
                c = "}"
            elif number < 1000:
                c = "{"
            else:
                c = "@"
            n = data["%02d%02d" % (i+1, day)].replace(w, "%c%d" % (c, number))
            if n != data["%02d%02d" % (i+1, day)]:
                count += 1
                saved += len(data["%02d%02d" % (i+1, day)]) - len(n)
                data["%02d%02d" % (i+1, day)] = n
    return data, count, saved

MAX_UNCOMPRESSED_APLITE = 8000
MAX_UNCOMPRESSED_BASALT = 50000

def yearday(m, d): # 0 = January

    n = 0
    for i in range(m-1):
        n += month_lengths[i]

    return n + d - 1


def join_small(data, target):

    res = 0
    newdata = {}
    where_is = {}

    curr_day = None
    curr = ""

    if target == "aplite":
        max_uncompressed = MAX_UNCOMPRESSED_APLITE
    else:
        max_uncompressed = MAX_UNCOMPRESSED_BASALT

    for i in range(len(month_lengths)):
        for day in range(1, month_lengths[i]+1):

            m = "%02d%02d" % (i+1, day)
            for k in newdata:
                if len(newdata[k].encode()) + len(data[m].encode()) < max_uncompressed:
                    where_is[yearday(i+1, day)] = {"res_idx":k, "num_events":len(data[m].split("\n")) - 1, "offset":len(newdata[k].encode())}
                    newdata[k] += data[m]
                    break
            else:
                newdata[res] = data[m]
                where_is[yearday(i+1, day)] = {"res_idx":res, "num_events":len(data[m].split("\n")) - 1, "offset":0}
                res += 1

    return newdata, where_is


def save(data, fout, path, target, debug):
    compressed_size = 0
    size = 0
    biggest = 0

    if debug:
        os.makedirs("debug_dumps", exist_ok=True)
    fout.write("static const unsigned short uncompressed_res_size[] = {\n")
    for c in data:
        d = zlib.compress(data[c].encode(), 9)
        size += len(data[c].encode())
        fout.write("\t%d,\n" % len(data[c].encode()))
        if len(data[c].encode()) > biggest:
            biggest = len(data[c].encode())
        compressed_size += len(d) - 6
        if debug:
            open("debug_dumps/%03d.txt" % int(c), "w").write(data[c])
        open(os.path.join(path, "resources/%03d_%s.compressed" % (int(c), target)), "wb").write(d[2:-4])
    fout.write("};\n\n")
    fout.write("#define DECOMPRESS_BUFFER_SIZE %d\n" % (biggest+1))
    fout.write("unsigned char decompress_buffer[DECOMPRESS_BUFFER_SIZE];\n")
    return size, compressed_size

def update_json(path, target, num_res):

    try:
        package = json.loads(open(os.path.join(path, "package.json")).read())
    except:
        print("ERROR: can't read: %s" % os.path.join(path, "package.json"))
        sys.exit(3)

    new_media = []
    for m in package["pebble"]["resources"]["media"]:
        if m["type"] != "raw" or (m["type"] == "raw" and not target in m["targetPlatforms"]):
            new_media.append(m)

    if target == "basalt":
        t = ["basalt", "chalk"]
    else:
        t = [target]

    for i in range(num_res):
        m = {"type":"raw", 
             "name":"EVENT_%03d" % i, 
             "file":"%03d_%s.compressed" % (i, target),
             "targetPlatforms": t}
        new_media.append(m)
    
    package["pebble"]["resources"]["media"] = new_media
    open(os.path.join(path, "package.json"), "w").write(json.dumps(package, indent=4))


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description='Today in History event data set selector')
    parser.add_argument('--begin', type=int, required=True,
                        help='Event data set start year.')
    parser.add_argument('--end', type=int, required=True,
                        help='Event data set end year.')
    parser.add_argument('--lang', default='en', type=str,
                        help='Event data set language. "en" or "sv". Default is "en".')

    parser.add_argument('--target', default='basalt', type=str,
                        help='Target platform. "aplite" or "basalt". Default is "basalt".')

    parser.add_argument('--outdir', default='..', type=str,
                        help='Ouput directory where package.json exists. Default is "..".')

    parser.add_argument('--atleast-one', action='store_true',
                        help='Force at least one event per day, even if no event exist in the given period. Default no.')
    parser.add_argument('--only-one', action='store_true',
                        help='Force one event per day, if more the one event exists in the given period. Default no.')

    parser.add_argument('--exclude',  default='', type=str,
                        help='Exclude events that include the given string.')
    parser.add_argument('--debug', action='store_true',
                        help='Enable some debugging.')

    args = parser.parse_args()

    if args.end < args.begin:
        print("Error: End year must be bigger than start year.")
        sys.exit(1)

    if args.target != "basalt" and args.target != "aplite":
        print("Error: Target can only be aplite or basalt.")
        sys.exit(2)

    if args.lang != "sv" and args.lang != "en":
        print("Error: Only Swedish(sv) and English(en) are supported languages.")
        sys.exit(3)

    if not os.path.exists(os.path.join(args.outdir, "package.json")):
        print("Error: output directory does not include package.json")
        sys.exit(4)


    fname = os.path.join(args.outdir, "src/c/data_%s.inc" % args.target)
    try:
        fout = open(fname, "w")
    except:
        print("ERROR: Failed to open:", fname)
        sys.exit(1)

    fname = os.path.join(args.outdir, "src/c/about_%s.inc" % args.target)
    try:
        fabout = open(fname, "w")
    except:
        print("ERROR: Failed to open:", fname)
        sys.exit(1)


    fout.write("/*\n * AUTOGENERATED FILE: DO NOT EDIT!\n * Language: %s Target: %s Years: %s-%s\n */\n\n" % (args.lang, args.target, args.begin, args.end) )
    fout.write('#include <stdbool.h>\n')
    fout.write('#include <pebble.h>\n')
    fout.write('#include "compress_format.h"\n\n')
                
    fabout.write("/*\n * AUTOGENERATED FILE: DO NOT EDIT!\n */\n\n")

    data = load_data(args.lang)
    data, total_size, longest_line = only_events(data, args)

    words = analyze_words(data)

    dict_data = ""
    dict_offsets = []
    number = 0

    # Helps since aplite has only 24k for code+bss+data, ie uncompress buffer must be small.
    if args.target == "aplite":
        for w in sorted(words, key=words.get, reverse=True):
            if words[w] < 500:
                break

            data, count, saved = compress(data, w, number)

            if count > 0:
                dict_offsets.append(len(dict_data.encode()))
                dict_data += w
                number += 1
            if number > 2047:
                break

    data, where_is = join_small(data, args.target)

    size, compressed_size = save(data, fout, args.outdir, args.target, args.debug)

    if args.target == "aplite" and compressed_size > (128 - 4)*1024:
        print("ERROR: Too much data %d > %d to fit in aplite platform resource storage!" % (compressed_size, (128 - 4)*1024))
        sys.exit(2)

    if args.target == "basalt" and compressed_size > (256 - 4)*1024:
        print("ERROR: Too much data %d > %d to fit in basalt platform resource storage!" % (compressed_size, (256 - 4)*1024))
        sys.exit(2)

    fout.write("/* Raw data: %d Uncompressed with global dictionary: %d Compressed: %d ratio: %d%% */\n" % (total_size, size, compressed_size, 100*compressed_size // size))

    num_events = 0
    days_without_events = 0
    for k in where_is:
        if where_is[k]["num_events"] == 0:
            days_without_events += 1
        num_events += where_is[k]["num_events"]

    subjects = ["Data set creation date", "Language", "Era", "Number of events", "Days without events", "Average events per day", 
                "Data size compressed", "Data size original", "Compression ratio"]

    fabout.write("#define NUM_SUBJ %d\n\n" % len(subjects))

    fabout.write("static const char *subj_title[] = {\n")
    for s in subjects:
        fabout.write('\t"%s",\n' % s)
    fabout.write("};\n\n")

    fabout.write("static const char *subj_val[] = {\n")

    date_txt = datetime.datetime.now().strftime("%e %b %Y, %H:%M")
    fabout.write('\t"%s",\n' % date_txt)
    print("Date", date_txt)

    fabout.write('\t"%s",\n' % args.lang)
    print("Language:", args.lang)

    if args.begin < 0:
        if args.lang == 'en':
            start_year_txt = "%d BC" % -args.begin
        elif args.lang == "sv":
            start_year_txt = "%d f.Kr." % -args.begin
        else:
            print("ERROR: Unknown language:", args.lang)
            sys.exit(5)
    else:
        start_year_txt = "%d" % args.begin

    if args.end < 0:
        if args.lang == 'en':
            end_year_txt = "%d BC" % -args.end
        elif args.lang == "sv":
            end_year_txt = "%d f.Kr." % -args.end
        else:
            print("ERROR: Unknown language:", args.lang)
            sys.exit(5)
    else:
        end_year_txt = "%d" % args.end

    era_txt = "%s - %s" % (start_year_txt, end_year_txt)
    fabout.write('\t"%s",\n' % era_txt)
    print("Era:", era_txt)

    fabout.write('\t"%d",\n' % num_events)
    print("Number of events:", num_events)

    fabout.write('\t"%d",\n' % days_without_events)
    print("Days without events:", days_without_events)

    average_events_per_day_txt = "%.2f" % (num_events / 366)
    fabout.write('\t"%s",\n' % average_events_per_day_txt)
    print("Average events per day:", average_events_per_day_txt);

    compressed_txt = "%dk" % (compressed_size//1024)
    fabout.write('\t"%s",\n' % compressed_txt)
    print("Data size (compressed):", compressed_txt);

    uncompressed_txt = "%dk" % (total_size//1024)
    fabout.write('\t"%s",\n' % uncompressed_txt)
    print("Data size (uncompressed):", uncompressed_txt);

    ratio_txt = "%d%%" % (100*compressed_size // total_size)
    fabout.write('\t"%s",\n' % ratio_txt)
    print("Compression ratio:", ratio_txt)

    fabout.write("};\n")
    fabout.close()

    fout.write("static char out_buffer[%d];\n" % longest_line);
    fout.write("static const struct whereis whereis[] = {\n")
    i = 0
    for k in sorted(where_is):
        if i % 8 == 0:
            fout.write("\n\t")
        i += 1
        fout.write("{RESOURCE_ID_EVENT_%03d, %d, %d}, " % (where_is[k]["res_idx"], where_is[k]["num_events"], where_is[k]["offset"]))
    fout.write("\n};\n\n")

    fout.write("static const unsigned int dict_offsets[] = {")
    i = 0
    for d in dict_offsets:
        if i % 16 == 0:
            fout.write("\n\t")
        i += 1
        fout.write("%d," % d)
    fout.write("%d" % len(dict_data.encode())) # To ease size calculation
    fout.write("\n};\n\n")

    fout.write("static const unsigned char dict[] = {")
    i = 0
    for c in dict_data.encode():
        if i % 16 == 0:
            fout.write("\n\t")
        i += 1
        fout.write("0x%02x, " % c)
    fout.write("\n};\n\n")

    if args.lang == 'sv':
        fout.write('static const char bc_string[] = " f.Kr";\n')
        fout.write('static const char *month_strings[] = {"januari", "februari", "mars", "april", "maj", "juni", "juli", "augusti", "september", "oktober", "november", "december"};\n')
        fout.write('static const bool day_before_month = true;\n')
        fout.write('static const char noevent_string[] = "En händelselös dag.";\n')
    else:
        fout.write('static const char bc_string[] = " BC";\n')
        fout.write('static const char *month_strings[] = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};\n')
        fout.write('static const bool day_before_month = false;\n')
        fout.write('static const char noevent_string[] = "Nothing of interest occured at this day.";\n')
    # Must be enough max length month name + 4
    fout.write("static char date_buffer[30];\n")
    fout.close()

    update_json(args.outdir, args.target, len(data))
