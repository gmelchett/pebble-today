#!/usr/bin/env python3

import wikipedia

print("Downloading events of the day - Swedish")

wikipedia.set_lang("sv")
os.makedirs("rawdata-sv", exist_ok=True)

months = ("januari", "februari", "mars", "april", "maj", "juni", "juli", "augusti", "september", "oktober", "november", "december")
lengths = (31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31)

for i in range(len(months)):
    for day in range(1, lengths[i]+1):
        t = "%d %s" % (day, months[i])
        print(t)
        start = False
        e = ""
        for l in wikipedia.page(t).content.split("\n"):
            if start and l == '== Födda ==':
                break
            if start:
                e += l + "\n"
            if l == '== Händelser ==':
                start = True
        open("rawdata-sv/%02d%02d.txt" % (i+1, day), "w").write(e)
print("Done -- Lot of dates will require manual editing before the compress script can be used.")



