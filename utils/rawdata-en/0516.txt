218 – Julia Maesa, aunt of the assassinated Caracalla, is banished to her home in Syria by self-proclaimed emperor Macrinus. She declares her 14-year-old grandson Elagabalus to be emperor of Rome. Macrinus is later deposed.
946 – Emperor Suzaku abdicates the throne in favor of his brother Murakami who becomes the 62nd emperor of Japan.
1204 – Baldwin IX, Count of Flanders is crowned as the first Emperor of the Latin Empire.
1527 – The Florentines drive out the Medici for a second time and Florence re-establishes itself as a republic.
1532 – Sir Thomas More resigns as Lord Chancellor of England.
1568 – Mary, Queen of Scots, flees to England.
1584 – Santiago de Vera becomes sixth Governor-General of the Spanish colony of the Philippines.
1770 – A 14-year-old Marie Antoinette marries 15-year-old Louis-Auguste who later becomes king of France.
1771 – The Battle of Alamance, a pre-American Revolutionary War battle between local militia and a group of rebels called The "Regulators", occurs in present-day Alamance County, North Carolina.
1811 – Peninsular War: The allies Spain, Portugal and United Kingdom, defeat the French at the Battle of Albuera.
1812 – Russian Field Marshal Mikhail Kutuzov signs the Treaty of Bucharest, ending the Russo-Turkish War. Bessarabia is annexed by Imperial Russia.
1822 – Greek War of Independence: The Turks capture the Greek town of Souli.
1834 – The Battle of Asseiceira is fought, the last and decisive engagement of the Liberal Wars in Portugal.
1843 – The first major wagon train heading for the Pacific Northwest sets out on the Oregon Trail with one thousand pioneers from Elm Grove, Missouri.
1866 – The United States Congress establishes the nickel.
1868 – United States President Andrew Johnson is acquitted in his impeachment trial by one vote in the United States Senate.
1874 – A flood on the Mill River in Massachusetts destroys much of four villages and kills 139 people.
1888 – Nikola Tesla delivers a lecture describing the equipment which will allow efficient generation and use of alternating currents to transmit electric power over long distances.
1891 – The International Electrotechnical Exhibition opens in Frankfurt, Germany, and will feature the world's first long distance transmission of high-power, three-phase electric current (the most common form today).
1916 – The United Kingdom of Great Britain and Ireland and the French Third Republic sign the secret wartime Sykes-Picot Agreement partitioning former Ottoman territories such as Iraq and Syria.
1918 – The Sedition Act of 1918 is passed by the U.S. Congress, making criticism of the government during wartime an imprisonable offense. It will be repealed less than two years later.
1919 – A naval Curtiss NC-4 aircraft commanded by Albert Cushing Read leaves Trepassey, Newfoundland, for Lisbon via the Azores on the first transatlantic flight.
1920 – In Rome, Pope Benedict XV canonizes Joan of Arc.
1929 – In Hollywood, the first Academy Awards ceremony takes place.
1943 – The Holocaust: The Warsaw Ghetto Uprising ends.
1951 – The first regularly scheduled transatlantic flights begin between Idlewild Airport (now John F Kennedy International Airport) in New York City and Heathrow Airport in London, operated by El Al Israel Airlines.
1959 – The Triton Fountain is inaugurated in Valletta, Malta.
1960 – Theodore Maiman operates the first optical laser (a ruby laser), at Hughes Research Laboratories in Malibu, California.
1961 – Park Chung-hee leads a coup d'état to overthrow the Second Republic of South Korea.
1966 – The Communist Party of China issues the "May 16 Notice", marking the beginning of the Cultural Revolution.
1969 – Venera program: Venera 5, a Soviet space probe, lands on Venus.
1974 – Josip Broz Tito is elected president for life of Yugoslavia.
1983 – Sudan People's Liberation Army/Movement rebels against the Sudanese government.
1988 – A report by the Surgeon General of the United States C. Everett Koop states that the addictive properties of nicotine are similar to those of heroin and cocaine.
1991 – Queen Elizabeth II of the United Kingdom addresses a joint session of the United States Congress. She is the first British monarch to address the U.S. Congress.
1997 – Mobutu Sese Seko, the President of Zaire, flees the country.
2003 – In Morocco, 33 civilians are killed and more than 100 people are injured in the Casablanca terrorist attacks.
2005 – Kuwait permits women's suffrage in a 35–23 National Assembly vote.
2011 – STS-134 (ISS assembly flight ULF6), launched from the Kennedy Space Center on the 25th and final flight for Space Shuttle Endeavour.
2014 – Twelve people are killed in two explosions in the Gikomba market area of Nairobi, Kenya.


