43 BC – Marcus Tullius Cicero is assassinated.
574 – Byzantine Emperor Justin II retires due to recurring seizures of insanity. He abdicates the throne in favor of his general Tiberius, proclaiming him Caesar.
1703 – The Great Storm of 1703, the greatest windstorm ever recorded in the southern part of Great Britain, makes landfall. Winds gust up to 120 mph, and 9,000 people die.
1724 – Tumult of Thorn: Religious unrest is followed by the execution of nine Protestant citizens and the mayor of Thorn (Toruń) by Polish authorities.
1732 – The Royal Opera House opens at Covent Garden, London, England.
1776 – Gilbert du Motier, Marquis de Lafayette, arranges to enter the American military as a major general.
1787 – Delaware becomes the first state to ratify the United States Constitution.
1842 – First concert of the New York Philharmonic, founded by Ureli Corelli Hill.
1869 – American outlaw Jesse James commits his first confirmed bank robbery in Gallatin, Missouri.
1904 – Comparative fuel trials begin between warships HMS Spiteful and HMS Peterel: Spiteful was the first warship powered solely by fuel oil, and the trials led to the obsolescence of coal in ships of the Royal Navy.
1917 – World War I: The United States declares war on Austria-Hungary.
1922 – The Parliament of Northern Ireland votes to remain a part of the United Kingdom and not unify with Southern Ireland.
1930 – W1XAV in Boston, Massachusetts telecasts video from the CBS radio orchestra program, The Fox Trappers. The telecast also includes the first television commercial in the United States, an advertisement for I.J. Fox Furriers, who sponsored the radio show.
1936 – Australian cricketer Jack Fingleton becomes the first player to score centuries in four consecutive Test innings.
1941 – World War II: Attack on Pearl Harbor: The Imperial Japanese Navy carries out a surprise attack on the United States Pacific Fleet and its defending Army and Marine air forces at Pearl Harbor, Hawaii. (For Japan's near-simultaneous attacks on Eastern Hemisphere targets, see December 8.)
1946 – A fire at the Winecoff Hotel in Atlanta, Georgia kills 119 people, the deadliest hotel fire in U.S. history.
1949 – Chinese Civil War: The Government of the Republic of China moves from Nanking to Taipei, Taiwan.
1962 – Prince Rainier III of Monaco revises the principality's constitution, devolving some of his power to advisory and legislative councils.
1963 – Instant replay makes its debut during the Army-Navy football game in Philadelphia, Pennsylvania, United States.
1965 – Pope Paul VI and Patriarch Athenagoras I simultaneously revoke mutual excommunications that had been in place since 1054.
1971 – Pakistan President Yahya Khan announces the formation of a coalition government with Nurul Amin as Prime Minister and Zulfikar Ali Bhutto as Deputy Prime Minister.
1972 – Apollo 17, the last Apollo moon mission, is launched. The crew takes the photograph known as The Blue Marble as they leave the Earth.
1982 – In Texas, Charles Brooks, Jr., becomes the first person to be executed by lethal injection in the United States.
1983 – An Iberia Airlines Boeing 727 collides with an Aviaco DC-9 in dense fog while the two airliners are taxiing down the runway at Madrid–Barajas Airport, killing 93 people.
1987 – Pacific Southwest Airlines Flight 1771, a British Aerospace 146-200A, crashes near Paso Robles, California, killing all 43 on board, after a disgruntled passenger shoots his ex-boss traveling on the flight, then shoots both pilots and himself.
1988 – The 6.8 Ms Armenian earthquake shakes the northern part of the country with a maximum MSK intensity of X (Devastating), killing 25,000–50,000 and injuring 31,000–130,000.
1993 – Long Island Rail Road shooting: Passenger Colin Ferguson murders six people and injures 19 others on the LIRR in Nassau County, New York.
1995 – The Galileo spacecraft arrives at Jupiter, a little more than six years after it was launched by Space Shuttle Atlantis during Mission STS-34.
1999 – A&M Records, Inc. v. Napster, Inc.: The Recording Industry Association of America sues the peer-to-peer file-sharing service Napster, alleging copyright infringement.
2003 – The Conservative Party of Canada is officially registered, following the merger of the Canadian Alliance and the Progressive Conservative Party of Canada.
2005 – Rigoberto Alpizar, a passenger on American Airlines Flight 924 who allegedly claimed to have a bomb, is shot and killed by a team of U.S. federal air marshals at Miami International Airport.
2015 – The JAXA probe Akatsuki successfully enters orbit around Venus five years after the first attempt.
2016 – Syrian army continues large-scale attack and controls the revival (Sheikh Lutfi, Marja, Bab al-Nairab, Maadi, Al-Salhin) in the east of Aleppo backed by Russian Air Force and Iranian militias.


