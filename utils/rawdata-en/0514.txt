1264 – Battle of Lewes: Henry III of England is captured and forced to sign the Mise of Lewes, making Simon de Montfort the de facto ruler of England.
1509 – Battle of Agnadello: In northern Italy, French forces defeat the Venetians.
1607 – Jamestown, Virginia is settled as an English colony.
1608 – The Protestant Union is founded in Auhausen.
1610 – Henry IV of France is assassinated, bringing Louis XIII to the throne.
1643 – Four-year-old Louis XIV becomes King of France upon the death of his father, Louis XIII.
1747 – War of the Austrian Succession: A British fleet under Admiral George Anson defeats the French at the First Battle of Cape Finisterre.
1787 – In Philadelphia, delegates convene a Constitutional Convention to write a new Constitution for the United States; George Washington presides.
1796 – Edward Jenner administers the first smallpox inoculation.
1800 – The process of the U.S. Government moving the United States capital city from Philadelphia to Washington, D.C. begins.
1804 – The Lewis and Clark Expedition departs from Camp Dubois and begins its historic journey by traveling up the Missouri River.
1811 – Paraguay: Pedro Juan Caballero, Fulgencio Yegros and José Gaspar Rodríguez de Francia start actions to depose the Spanish governor
1836 – The Treaties of Velasco are signed in Velasco, Texas.
1863 – American Civil War: The Battle of Jackson takes place.
1868 – Boshin War: The Battle of Utsunomiya Castle ends as former Tokugawa shogunate forces withdraw northward to Aizu by way of Nikkō.
1870 – The first game of rugby in New Zealand is played in Nelson between Nelson College and the Nelson Rugby Football Club.
1878 – The last witchcraft trial held in the United States begins in Salem, Massachusetts, after Lucretia Brown, an adherent of Christian Science, accused Daniel Spofford of attempting to harm her through his mental powers.
1879 – The first group of 463 Indian indentured laborers arrives in Fiji aboard the Leonidas.
1913 – Governor of New York William Sulzer approves the charter for the Rockefeller Foundation, which begins operations with a $100 million donation from John D. Rockefeller.
1925 – Virginia Woolf's novel Mrs Dalloway is published.
1931 – Five unarmed civilians are killed in the Ådalen shootings, as the Swedish military is called in to deal with protesting workers.
1935 – The Philippines ratifies an independence agreement.
1939 – Lina Medina becomes the youngest confirmed mother in medical history at the age of five.
1940 – World War II: Rotterdam is bombed by the German Luftwaffe.
1940 – World War II: The Battle of the Netherlands ends with the Netherlands surrendering to Germany.
1943 – World War II: A Japanese submarine sinks AHS Centaur off the coast of Queensland.
1948 – Israel is declared to be an independent state and a provisional government is established. Immediately after the declaration, Israel is attacked by the neighboring Arab states, triggering the 1948 Arab–Israeli War.
1951 – Trains run on the Talyllyn Railway in Wales for the first time since preservation, making it the first railway in the world to be operated by volunteers.
1955 – Cold War: Eight Communist bloc countries, including the Soviet Union, sign a mutual defense treaty called the Warsaw Pact.
1961 – Civil Rights Movement: The Freedom Riders bus is fire-bombed near Anniston, Alabama, and the civil rights protesters are beaten by an angry mob.
1970 – Andreas Baader is freed from custody by Ulrike Meinhof, Gudrun Ensslin and others, a pivotal moment in the formation of The Red Army Faction.
1973 – Skylab, the United States' first space station, is launched.
1988 – Carrollton bus collision: A drunk driver traveling the wrong way on Interstate 71 near Carrollton, Kentucky, United States hits a converted school bus carrying a church youth group. Twenty-seven die in the crash and ensuing fire.
2004 – The Constitutional Court of South Korea overturns the impeachment of President Roh Moo-hyun.
2012 – Agni Air Flight CHT crashes near Jomsom Airport in Jomsom, Nepal, after a failed go-around, killing 15 people.


