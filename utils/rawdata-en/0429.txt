1091 – Battle of Levounion: The Pechenegs are defeated by Byzantine Emperor Alexios I Komnenos.
1386 – Battle of the Vikhra River: The Principality of Smolensk is defeated by the Grand Duchy of Lithuania and becomes its vassal.
1429 – Joan of Arc arrives to relieve the Siege of Orléans.
1483 – Gran Canaria, the main island of the Canary Islands, is conquered by the Kingdom of Castile.
1521 – Swedish War of Liberation: Swedish troops defeat a Danish force in the Battle of Västerås.
1770 – James Cook arrives in Australia at Botany Bay, which he names.
1781 – American Revolutionary War: British and French ships clash in the Battle of Fort Royal off the coast of Martinique.
1834 – Charles Darwin during the second survey voyage of HMS Beagle, ascended the Bell mountain, Cerro La Campana on 17 August 1834, his visit being commemorated by a memorial plaque.
1861 – American Civil War: Maryland's House of Delegates votes not to secede from the Union.
1862 – American Civil War: The Capture of New Orleans by Union forces under David Farragut.
1864 – Theta Xi fraternity is founded at Rensselaer Polytechnic Institute, the only fraternity to be founded during the American Civil War.
1903 – A 30 million cubic-metre landslide kills 70 people in Frank, in the District of Alberta, Canada.
1910 – The Parliament of the United Kingdom passes the People's Budget, the first budget in British history with the expressed intent of redistributing wealth among the British public.
1911 – Tsinghua University, one of mainland China's leading universities, is founded.
1916 – World War I: The UK's 6th Indian Division surrenders to Ottoman Forces at the Siege of Kut in one of the largest surrenders of British forces up to that point.
1916 – Easter Rising: After six days of fighting, Irish rebel leaders surrender to British forces in Dublin, bringing the Easter Rising to an end.
1944 – World War II: British agent Nancy Wake, a leading figure in the French Resistance and the Gestapo's most wanted person, parachutes back into France to be a liaison between London and the local maquis group.
1945 – World War II: The German army in Italy unconditionally surrenders to the Allies.
1945 – World War II: Start of Operation Manna.
1945 – World War II: The Captain-class frigate HMS Goodall (K479) is torpedoed by U-286 outside the Kola Inlet becoming the last Royal Navy ship to be sunk in the European theatre of World War II.
1945 – World War II: Führerbunker: Adolf Hitler marries his longtime partner Eva Braun in a Berlin bunker and designates Admiral Karl Dönitz as his successor; Hitler and Braun both commit suicide the following day.
1945 – Dachau concentration camp is liberated by United States troops.
1945 – The Italian commune of Fornovo di Taro is liberated from German forces by Brazilian forces.
1946 – The International Military Tribunal for the Far East convenes and indicts former Prime Minister of Japan Hideki Tojo and 28 former Japanese leaders for war crimes.
1951 – Tibetan delegates to the Central People's Government arrive in Beijing and draft a Seventeen Point Agreement for Chinese sovereignty and Tibetan autonomy.
1953 – The first U.S. experimental 3D television broadcast showed an episode of Space Patrol on Los Angeles ABC affiliate KECA-TV.
1965 – Pakistan's Space and Upper Atmosphere Research Commission (SUPARCO) successfully launches its seventh rocket in its Rehber series.
1967 – After refusing induction into the United States Army the previous day, Muhammad Ali is stripped of his boxing title.
1968 – The controversial musical Hair, a product of the hippie counter-culture and sexual revolution of the 1960s, opens at the Biltmore Theatre on Broadway, with some of its songs becoming anthems of the anti-Vietnam War movement.
1970 – Vietnam War: United States and South Vietnamese forces invade Cambodia to hunt Viet Cong.
1974 – Watergate scandal: United States President Richard Nixon announces the release of edited transcripts of White House tape recordings relating to the scandal.
1975 – Vietnam War: Operation Frequent Wind: The U.S. begins to evacuate U.S. citizens from Saigon before an expected North Vietnamese takeover. U.S. involvement in the war comes to an end.
1975 – Vietnam War: The North Vietnamese army completes its capture of all parts of South Vietnamese-held Trường Sa Islands.
1986 – A fire at the Central library of the City of Los Angeles Public Library damages or destroys 400,000 books and other items.
1986 – Chernobyl disaster: American and European spy satellites capture the ruins of the 4th Reactor at the Chernobyl Power Plant
1991 – A cyclone strikes the Chittagong district of southeastern Bangladesh with winds of around 155 miles per hour (249 km/h), killing at least 138,000 people and leaving as many as ten million homeless.
1991 – The 7.0 Mw Racha earthquake affects Georgia with a maximum MSK intensity of IX (Destructive), killing 270 people.
1992 – Los Angeles riots: Riots in Los Angeles, following the acquittal of police officers charged with excessive force in the beating of Rodney King. Over the next three days 63 people are killed and hundreds of buildings are destroyed.
1997 – The Chemical Weapons Convention of 1993 enters into force, outlawing the production, stockpiling and use of chemical weapons by its signatories.
2011 – The Wedding of Prince William and Catherine Middleton takes place at Westminster Abbey in London.
2013 – A powerful explosion occurs in an office building in Prague, believed to have been caused by natural gas, injures 43 people.
2015 – A baseball game between the Baltimore Orioles and the Chicago White Sox sets the all-time low attendance mark for Major League Baseball. Zero fans were in attendance for the game, as the stadium was officially closed to the public due to the 2015 Baltimore protests.


