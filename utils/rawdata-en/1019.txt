202 BC – Second Punic War: At the Battle of Zama, Roman legions under Scipio Africanus defeat Hannibal Barca, leader of the army defending Carthage.
439 – The Vandals, led by King Gaiseric, take Carthage in North Africa.
1216 – King John of England dies at Newark-on-Trent and is succeeded by his nine-year-old son Henry.
1386 – The Universität Heidelberg holds its first lecture, making it the oldest German university.
1453 – The Hundred Years' War ends with the French recapture of Bordeaux, leaving English control only on Calais
1466 – The Thirteen Years' War ends with the Second Treaty of Thorn.
1469 – Ferdinand II of Aragon marries Isabella I of Castile, a marriage that paves the way to the unification of Aragon and Castile into a single country, Spain.
1512 – Martin Luther becomes a doctor of theology (Doctor in Biblia).
1649 – New Ross town, County Wexford, Ireland, surrenders to Oliver Cromwell.
1781 – At Yorktown, Virginia, representatives of British commander Lord Cornwallis hand over Cornwallis' sword and formally surrender to George Washington and the comte de Rochambeau.
1789 – John Jay is sworn in as the first Chief Justice of the United States.
1805 – Napoleonic Wars: Austrian General Mack surrenders his army to the Grande Armée of Napoleon Bonaparte at the Battle of Ulm; 30,000 prisoners are captured and 10,000 casualties inflicted on the losers.
1812 – Napoleon Bonaparte retreats from Moscow.
1813 – The Battle of Leipzig concludes, giving Napoleon Bonaparte one of his worst defeats.
1822 – In Parnaíba; Simplício Dias da Silva, João Cândido de Deus e Silva and Domingos Dias declare the independent state of Piauí.
1864 – Battle of Cedar Creek: A U.S. Army force under Philip Sheridan destroys a Confederate army under Jubal Early.
1864 – St. Albans Raid: Confederate raiders launch an attack on Saint Albans, Vermont from Canada.
1866 – Austria cedes Veneto and Mantua to France, which immediately awards them to Italy in exchange for the earlier Italian acquiescence to the French annexation of Savoy and Nice.
1900 – Max Planck discovers the law of black-body radiation (Planck's law).
1912 – Italy takes possession of Tripoli, Libya from the Ottoman Empire.
1914 – The First Battle of Ypres begins.
1921 – Portuguese Prime Minister António Granjo and other politicians are murdered in a Lisbon coup.
1922 – British Conservative MPs meeting at the Carlton Club vote to break off the Coalition Government with David Lloyd George of the Liberal Party.
1933 – Germany withdraws from the League of Nations.
1935 – The League of Nations places economic sanctions on fascist Italy for its invasion of Ethiopia.
1943 – The cargo vessel Sinfra is attacked by Allied aircraft at Souda Bay, Crete, and sunk; 2,098 Italian prisoners of war drown with it.
1943 – Streptomycin, the first antibiotic remedy for tuberculosis, is isolated by researchers at Rutgers University.
1944 – United States forces land in the Philippines.
1944 – A coup is launched against Juan Federico Ponce Vaides, beginning the ten-year Guatemalan Revolution
1950 – The People's Liberation Army takes control of the town of Chamdo; this is sometimes called the "Invasion of Tibet".
1950 – The People's Republic of China joins the Korean War by sending thousands of troops across the Yalu River to fight United Nations forces.
1950 – Iran becomes the first country to accept technical assistance from the United States under the Point Four Program.
1956 – The Soviet Union and Japan sign a Joint Declaration, officially ending the state of war between the two countries that had existed since August 1945.
1960 – Cold War: The United States government imposes a near-total trade embargo against Cuba.
1973 – President Richard Nixon rejects an Appeals Court decision that he turn over the Watergate tapes.
1974 – Niue becomes a self-governing colony of New Zealand.
1984 – Roman Catholic priest from Poland, Jerzy Popiełuszko, associated with the Solidarity Union, is murdered by three agents of the Polish Communist internal intelligence agency.
1986 – Samora Machel, President of Mozambique and a prominent leader of FRELIMO, and 33 others die when their Tupolev Tu-134 plane crashes into the Lebombo Mountains.
1987 – The United States Navy conducts Operation Nimble Archer, an attack on two Iranian oil platforms in the Persian Gulf.
1987 – Black Monday: The Dow Jones Industrial Average falls by 22%, 508 points.
1988 – The British government imposes a broadcasting ban on television and radio interviews with members of Sinn Féin and eleven Irish republican and Ulster loyalist paramilitary groups.
1989 – The convictions of the Guildford Four are quashed by the Court of Appeal of England and Wales, after they had spent 15 years in prison.
2001 – SIEV X, an Indonesian fishing boat en route to Christmas Island, carrying over 400 asylum seekers, sinks in international waters with the loss of 353 people.
2003 – Mother Teresa is beatified by Pope John Paul II.
2005 – Saddam Hussein goes on trial in Baghdad for crimes against humanity.
2005 – Hurricane Wilma becomes the most intense Atlantic hurricane on record with a minimum pressure of 882 mb.
2012 – A bomb explosion kills eight people and injures 110 people in Beirut, Lebanon.
2013 – At least 105 people are injured in a train crash at the Once railway station in Buenos Aires.


