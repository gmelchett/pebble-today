1373 – Julian of Norwich has visions which are later transcribed in her Revelations of Divine Love.
1515 – Mary Tudor, Queen of France, and Charles Brandon, 1st Duke of Suffolk, are officially married at Greenwich.
1568 – Battle of Langside: The forces of Mary, Queen of Scots, are defeated by a confederacy of Scottish Protestants under James Stewart, Earl of Moray, her half-brother.
1619 – Dutch statesman Johan van Oldenbarnevelt is executed in The Hague after being convicted of treason.
1779 – War of the Bavarian Succession: Russian and French mediators at the Congress of Teschen negotiate an end to the war. In the agreement Austria receives the part of its territory that was taken from it (the Innviertel).
1780 – The Cumberland Compact is signed by leaders of the settlers in early Tennessee.
1787 – Captain Arthur Phillip leaves Portsmouth, England, with eleven ships full of convicts (the "First Fleet") to establish a penal colony in Australia.
1804 – Forces sent by Yusuf Karamanli of Tripoli to retake Derna from the Americans attack the city.
1830 – Ecuador gains its independence from Gran Colombia.
1846 – Mexican–American War: The United States declares war on Mexico.
1861 – American Civil War: Queen Victoria of the United Kingdom issues a "proclamation of neutrality" which recognizes the breakaway states as having belligerent rights.
1861 – The Great Comet of 1861 is discovered by John Tebbutt of Windsor, New South Wales, Australia.
1861 – Pakistan's (then a part of British India) first railway line opens, from Karachi to Kotri.
1862 – The USS Planter, a steamer and gunship, steals through Confederate lines and is passed to the Union, by a southern slave, Robert Smalls, who later was officially appointed as captain, becoming the first black man to command a United States ship.
1864 – American Civil War: Battle of Resaca: The battle begins with Union General Sherman fighting toward Atlanta.
1865 – American Civil War: Battle of Palmito Ranch: In far south Texas, the last land battle of the Civil War ends with a Confederate victory.
1880 – In Menlo Park, New Jersey, Thomas Edison performs the first test of his electric railway.
1888 – With the passage of the Lei Áurea ("Golden Law"), Empire of Brazil abolishes slavery.
1909 – The first Giro d'Italia starts from Milan. Italian cyclist Luigi Ganna will be the winner.
1912 – The Royal Flying Corps, the forerunner of the Royal Air Force, is established in the United Kingdom.
1917 – Three children report the first apparition of Our Lady of Fátima in Fátima, Portugal.
1939 – The first commercial FM radio station in the United States is launched in Bloomfield, Connecticut. The station later becomes WDRC-FM.
1940 – World War II: Germany's conquest of France begins as the German army crosses the Meuse. Winston Churchill makes his "blood, toil, tears, and sweat" speech to the House of Commons.
1940 – Queen Wilhelmina of the Netherlands flees her country to Great Britain after the German invasion. Princess Juliana takes her children to Canada for their safety.
1941 – World War II: Yugoslav royal colonel Dragoljub Mihailović starts fighting with German occupation troops, beginning the Serbian resistance.
1943 – World War II: German Afrika Korps and Italian troops in North Africa surrender to Allied forces.
1948 – Arab–Israeli War: The Kfar Etzion massacre is committed by Arab irregulars, the day before the declaration of independence of the state of Israel on May 14.
1950 – The first round of the Formula One World Championship is held at Silverstone.
1951 – The 400th anniversary of the founding of the National University of San Marcos is commemorated by the opening of the first large-capacity stadium in Peru.
1952 – The Rajya Sabha, the upper house of the Parliament of India, holds its first sitting.
1954 – The anti-National Service Riots, by Chinese middle school students in Singapore, take place.
1954 – The original Broadway production of The Pajama Game opens and runs for another 1,063 performances. Later received three Tony Awards for Best Musical, Best Performance by a Featured Actress in a Musical, and Best Choreography.
1958 – During a visit to Caracas, Venezuela, Vice President Richard Nixon's car is attacked by anti-American demonstrators.
1958 – May 1958 crisis: A group of French military officers lead a coup in Algiers demanding that a government of national unity be formed with Charles de Gaulle at its head in order to defend French control of Algeria.
1958 – Ben Carlin becomes the first (and only) person to circumnavigate the world by amphibious vehicle, having travelled over 17,000 kilometres (11,000 mi) by sea and 62,000 kilometres (39,000 mi) by land during a ten-year journey.
1960 – Hundreds of University of California, Berkeley students congregate for the first day of protest against a visit by the House Committee on Un-American Activities.
1967 – Dr. Zakir Husain becomes the third President of India. He is the first Muslim President of the Indian Union. He holds this position until August 24, 1969.
1971 – Over 900 unarmed Bengali Hindus are murdered in the Demra massacre.
1972 – Faulty electrical wiring ignites a fire underneath the Playtown Cabaret in Osaka, Japan. Blocked exits and non-functional elevators lead to 118 fatalities, with many victims leaping to their deaths.
1972 – The Troubles: A car bombing outside a crowded pub in Belfast sparks a two-day gun battle involving the Provisional IRA, Ulster Volunteer Force and British Army. Seven people are killed and over 66 injured.
1980 – An F3 tornado hits Kalamazoo County, Michigan. President Jimmy Carter declares it a federal disaster area.
1981 – Mehmet Ali Ağca attempts to assassinate Pope John Paul II in St. Peter's Square in Rome. The Pope is rushed to the Agostino Gemelli University Polyclinic to undergo emergency surgery and survives.
1985 – Police release a bomb on MOVE headquarters in Philadelphia to end a stand-off, killing six adults and five children, and destroying the homes of 250 city residents.
1989 – Large groups of students occupy Tiananmen Square and begin a hunger strike.
1990 – The Dinamo–Red Star riot took place at Maksimir Stadium in Zagreb, Croatia between the Bad Blue Boys (fans of Dinamo Zagreb) and the Delije (fans of Red Star Belgrade).
1992 – Li Hongzhi gives the first public lecture on Falun Gong in Changchun, People's Republic of China.
1995 – Alison Hargreaves, a 33-year-old British mother, becomes the first woman to conquer Everest without oxygen or the help of sherpas.
1996 – Severe thunderstorms and a tornado in Bangladesh kill 600 people.
1998 – Race riots break out in Jakarta, Indonesia, where shops owned by Indonesians of Chinese descent are looted and women raped.
1998 – India carries out two nuclear tests at Pokhran, following the three conducted on May 11. The United States and Japan impose economic sanctions on India.
2003 – City of Miami Gardens, Florida is incorporated.
2005 – Andijan uprising, Uzbekistan; Troops open fire on crowds of protestors after a prison break; at least 187 people were killed according to official estimates.
2006 – São Paulo violence: Rebellions occurs in several prisons in Brazil.
2011 – Two bombs explode in the Charsadda District of Pakistan killing 98 people and wounding 140 others.
2012 – Forty-nine dismembered bodies are discovered by Mexican authorities on Mexican Federal Highway 40.
2014 – An explosion at an underground coal mine in south-western Turkey kills 301 miners.


