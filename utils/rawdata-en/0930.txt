489 – Battle of Verona: The Ostrogoths under king Theoderic the Great defeat the forces of Odoacer for the second time at Verona (Northern Italy).
737 – Battle of the Baggage: Turgesh drive back an Umayyad invasion of Khuttal, follow them south of the Oxus and capture their baggage train.
1399 – Henry IV is proclaimed King of England.
1541 – Spanish conquistador Hernando de Soto and his forces enter Tula territory in present-day western Arkansas, encountering fierce resistance.
1551 – Tainei-ji incident: A coup in Yamaguchi by the military establishment of the Ōuchi clan forces their lord Ōuchi Yoshitaka to commit suicide, and the city is burned.
1744 – France and Spain defeat the Kingdom of Sardinia at the Battle of Madonna dell'Olmo.
1791 – The first performance of The Magic Flute, the last opera by Mozart to make its debut, took place at Freihaus-Theater auf der Wieden in Vienna, Austria.
1791 – The National Constituent Assembly in Paris is dissolved; Parisians hail Maximilien Robespierre and Jérôme Pétion as "incorruptible patriots".
1813 – Battle of Bárbula: Simón Bolívar defeats Santiago Bobadilla.
1860 – Britain's first tram service begins in Birkenhead, Merseyside.
1882 – Thomas Edison's first commercial hydroelectric power plant (later known as Appleton Edison Light Company) begins operation on the Fox River in Appleton, Wisconsin, United States.
1888 – Jack the Ripper kills his third and fourth victims, Elizabeth Stride and Catherine Eddowes.
1903 – The new Gresham's School is officially opened by Field Marshal Sir Evelyn Wood.
1906 – The Royal Galician Academy, Galician language's biggest linguistic authority, starts working in Havana.
1907 – McKinley National Memorial, the final resting place of assassinated U.S. President William McKinley and his family, is dedicated in Canton, Ohio.
1909 – Cunard Line’s RMS Mauretania (1906) makes a record breaking Westbound crossing that wins it the Blue Riband, which it would keep for 20 years.
1915 – Radoje Ljutovac becomes the first soldier in history to shoot down an enemy aircraft with ground-to-air fire.
1922 – University of Alabama opened the football season with a 110–0 victory over the Marion Military Institute which still stands as the school record for largest margin of victory and as their only 100 point game.
1927 – Babe Ruth becomes the first baseball player to hit 60 home runs in a season.
1931 – Start of "Die Voortrekkers" youth movement for Afrikaners in Bloemfontein, South Africa.
1935 – The Hoover Dam, astride the border between the U.S. states of Arizona and Nevada, is dedicated.
1938 – Britain, France, Germany and Italy sign the Munich Agreement, allowing Germany to occupy the Sudetenland region of Czechoslovakia.
1938 – The League of Nations unanimously outlaws "intentional bombings of civilian populations".
1939 – World War II: General Władysław Sikorski becomes commander-in-chief of the Polish Government in exile.
1939 – NBC broadcasts the first televised American football game between the Waynesburg Yellow Jackets and the Fordham Rams. Fordham won the game 34–7.
1941 – World War II: Holocaust in Kiev, Ukraine: German Einsatzgruppe C complete Babi Yar massacre.
1943 – The United States Merchant Marine Academy (USMMA) at Kings Point, New York was dedicated by President Franklin D. Roosevelt.
1945 – The Bourne End rail crash, in Hertfordshire, England, kills 43
1947 – The World Series, featuring the New York Yankees and the Brooklyn Dodgers, is televised for the first time.
1947 – Pakistan joins the United Nations
1949 – The Berlin Airlift ends.
1954 – The U.S. Navy submarine USS Nautilus is commissioned as the world's first nuclear reactor powered vessel.
1962 – Mexican-American labor leader César Chávez founds the National Farm Workers Association, which later becomes United Farm Workers.
1962 – James Meredith enters the University of Mississippi, defying segregation.
1965 – The Lockheed L-100, the civilian version of the C-130 Hercules, is introduced.
1965 – The 30 September Movement attempts a coup against the Indonesian government, which is crushed by the military under Suharto and leads to a mass anti-communist purge, with over 500,000 people killed.
1966 – The British protectorate of Bechuanaland declares its independence, and becomes the Republic of Botswana. Seretse Khama takes office as the first President.
1967 – BBC Light Programme, Third Programme and Home Service are replaced with BBC Radio 2, 3 and 4 Respectively, BBC Radio 1 is also launched with Tony Blackburn presenting its first show.
1968 – The Boeing 747 is rolled out and shown to the public for the first time at the Boeing Everett Factory.
1970 – Jordan makes a deal with the Popular Front for the Liberation of Palestine (PFLP) for the release of the remaining hostages from the Dawson's Field hijackings.
1972 – Roberto Clemente records the 3,000th and final hit of his career.
1975 – The Hughes (later McDonnell Douglas, now Boeing) AH-64 Apache makes its first flight. Eight years later, the first production model rolled out of the assembly line.
1977 – Because of US budget cuts and dwindling power reserves, the Apollo program's ALSEP experiment packages left on the Moon are shut down.
1979 – The Hong Kong MTR commences service with the opening of its Modified Initial System (aka. Kwun Tong Line).
1980 – Ethernet specifications are published by Xerox working with Intel and Digital Equipment Corporation.
1988 – Al Holbert was fatally injured when his privately owned propeller driven Piper PA-60 aircraft crashed shortly after takeoff near Columbus, Ohio when a clamshell door was not closed.
1990 – The Dalai Lama unveils the Canadian Tribute to Human Rights in Canada's capital city of Ottawa.
1993 – The 6.2 Mw Latur earthquake shakes Maharashtra, India with a maximum Mercalli intensity of VIII (Severe) killing 9,748 and injuring 30,000.
1994 – Aldwych tube station (originally Strand Station) of the London Underground closes after eighty-eight years in service.
1994 – Ongar railway station, the furthest London Underground from Central London, closes.
1999 – The Tokaimura nuclear accident causes the deaths of two technicians in Japan's second-worst nuclear accident.
2004 – The AIM-54 Phoenix, the primary missile for the F-14 Tomcat, is retired from service. Almost two years later, the Tomcat is retired.
2005 – The controversial drawings of Muhammad are printed in the Danish newspaper Jyllands-Posten.
2009 – The 7.6 Mw Sumatra earthquake shakes central Sumatra with a maximum Mercalli intensity of VIII (Severe). This dip-slip (reverse) earthquake left 1,115 people dead, and was followed several days later by a 6.6 Mw strike-slip event.
2016 – Hurricane Matthew became a Category 5 hurricane, making it the strongest hurricane to form in the Caribbean Sea, since Hurricane Felix in 2007.
2017 – Titus Zeman SDB. - priest, beatification ceremony in Bratislava, Slovakia.


