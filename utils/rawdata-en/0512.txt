254 – Pope Stephen I succeeds Pope Lucius I as the 23rd pope.
907 – Zhu Wen forces Emperor Ai into abdicating, ending the Tang dynasty after nearly three hundred years of rule.
1191 – Richard I of England marries Berengaria of Navarre who is crowned Queen consort of England the same day.
1328 – Antipope Nicholas V, a claimant to the papacy, is consecrated in Rome by the Bishop of Venice.
1364 – Jagiellonian University, the oldest university in Poland, is founded in Kraków, Poland.
1510 – The Prince of Anhua rebellion begins when Zhu Zhifan kills all the officials invited to a banquet and declares his intent on ousting the powerful Ming dynasty eunuch Liu Jin during the reign of the Zhengde Emperor.
1551 – National University of San Marcos, the oldest university in the Americas, is founded in Lima, Peru.
1588 – French Wars of Religion: Henry III of France flees Paris after Henry I, Duke of Guise enters the city and a spontaneous uprising occurs.
1593 – London playwright Thomas Kyd is arrested and tortured by the Privy Council for libel.
1619 – Dutch statesman Johan van Oldenbarnevelt is sentenced to death for high treason.
1689 – King William's War: William III of England joins the League of Augsburg starting a war with France.
1743 – Maria Theresa of Austria is crowned Queen of Bohemia after defeating her rival, Charles VII, Holy Roman Emperor.
1780 – American Revolutionary War: In the largest defeat of the Continental Army, Charleston, South Carolina is taken by British forces.
1784 – The Treaty of Paris signed on September 3, 1783, takes effect on this date.
1797 – War of the First Coalition: Napoleon I of France conquers Venice.
1821 – The first major battle of the Greek War of Independence against the Turks is fought in Valtetsi.
1846 – The Donner Party of pioneers departs Independence, Missouri for California, on what will become a year-long journey of hardship and cannibalism.
1862 – American Civil War: U.S. federal troops occupy Baton Rouge, Louisiana.
1863 – American Civil War: Battle of Raymond: Two divisions of James B. McPherson's XVII Corps turn the left wing of Confederate General John C. Pemberton's defensive line on Fourteen Mile Creek, opening up the interior of Mississippi to the Union Army during the Vicksburg Campaign.
1864 – American Civil War: The Battle of Spotsylvania Court House: Thousands of Union and Confederate soldiers die in "the Bloody Angle".
1865 – American Civil War: The Battle of Palmito Ranch: The first day of the last major land action to take place during the Civil War, resulting in a Confederate victory.
1870 – The Manitoba Act is given the Royal Assent, paving the way for Manitoba to become a province of Canada on July 15.
1881 – In North Africa, Tunisia becomes a French protectorate.
1885 – North-West Rebellion: The four-day Battle of Batoche, pitting rebel Métis against the Canadian government, comes to an end with a decisive rebel defeat.
1888 – In Southeast Asia, the North Borneo Chartered Company's territories become the British protectorate of North Borneo.
1926 – The Italian-built airship Norge becomes the first vessel to fly over the North Pole.
1932 – Ten weeks after his abduction, the infant son of Charles Lindbergh, Charles Jr., is found dead in Hopewell, New Jersey, just a few miles from the Lindberghs' home.
1933 – The Agricultural Adjustment Act is enacted to restrict agricultural production by paying farmers subsidies.
1937 – The Duke and Duchess of York are crowned as King George VI and Queen Elizabeth of the United Kingdom of Great Britain and Northern Ireland in Westminster Abbey.
1941 – Konrad Zuse presents the Z3, the world's first working programmable, fully automatic computer, in Berlin.
1942 – World War II: Second Battle of Kharkov: In eastern Ukraine, Red Army forces under Marshal Semyon Timoshenko launch a major offensive from the Izium bridgehead, only to be encircled and destroyed by the troops of Army Group South two weeks later.
1942 – World War II: The U.S. tanker SS Virginia is torpedoed in the mouth of the Mississippi River by the German submarine U-507.
1948 – Wilhelmina, Queen regnant of the Kingdom of the Netherlands, cedes the throne.
1949 – The Soviet Union lifts its blockade of Berlin.
1949 – The western occupying powers approve the Basic Law for the new German state: the Federal Republic of Germany.
1965 – The Soviet spacecraft Luna 5 crashes on the Moon.
1968 – Vietnam War: North Vietnamese and Viet Cong forces attack Australian troops defending Fire Support Base Coral.
1978 – In Zaire, rebels occupy the city of Kolwezi, the mining center of the province of Shaba (now known as Katanga); the local government asks the US, France and Belgium to restore order.
1981 – Francis Hughes, Provisional IRA hunger striker, dies in the Maze Prison, Northern Ireland.
1982 – During a procession outside the shrine of the Virgin Mary in Fátima, Portugal, security guards overpower Juan María Fernández y Krohn before he can attack Pope John Paul II with a bayonet.
1989 – The San Bernardino train disaster kills four people. A week later an underground gasoline pipeline explodes killing two more people.
1998 – Four students are shot at Trisakti University, leading to widespread riots and the fall of Suharto.
2002 – Former US President Jimmy Carter arrives in Cuba for a five-day visit with Fidel Castro, becoming the first President of the United States, in or out of office, to visit the island since Castro's 1959 revolution.
2003 – The Riyadh compound bombings, carried out by al-Qaeda, kill 26 people.
2006 – Mass unrest by the Primeiro Comando da Capital begins in São Paulo (Brazil), leaving at least 150 dead.
2006 – Iranian Azeris interpret a cartoon published in an Iranian magazine as insulting, resulting in massive riots throughout the country.
2008 – An earthquake (measuring around 8.0 magnitude) occurs in Sichuan, China, killing over 69,000 people.
2008 – U.S. Immigration and Customs Enforcement conducts the largest-ever raid of a workplace in Postville, Iowa, arresting nearly 400 immigrants for identity theft and document fraud.
2015 – A train derailment in Philadelphia kills eight people and injures more than 200.
2015 – Massive Nepal earthquake kills 218 people and injures more than 3500.
2017 – A ransomware attack attacks over 400 thousand computers worldwide, targeting computers of the UK'S National Health Services and Telefónica computers.


