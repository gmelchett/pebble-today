AD 69 – Otho seizes power in Rome, proclaiming himself Emperor of Rome, but rules for only three months before committing suicide.
1541 – King Francis I of France gives Jean-François Roberval a commission to settle the province of New France (Canada) and provide for the spread of the "Holy Catholic faith".
1559 – Elizabeth I is crowned Queen of England in Westminster Abbey, London, England.
1582 – Truce of Yam-Zapolsky: Russia cedes Livonia to the Polish–Lithuanian Commonwealth.
1759 – The British Museum opens.
1777 – American Revolutionary War: New Connecticut (present-day Vermont) declares its independence.
1782 – Superintendent of Finance Robert Morris goes before the U.S. Congress to recommend establishment of a national mint and decimal coinage.
1815 – War of 1812: American frigate USS President, commanded by Commodore Stephen Decatur, is captured by a squadron of four British frigates.
1822 – Greek War of Independence: Demetrios Ypsilantis is elected president of the legislative assembly.
1844 – University of Notre Dame receives its charter from the state of Indiana.
1865 – American Civil War: Fort Fisher in North Carolina falls to the Union, thus cutting off the last major seaport of the Confederacy.
1870 – A political cartoon for the first time symbolizes the Democratic Party with a donkey ("A Live Jackass Kicking a Dead Lion" by Thomas Nast for Harper's Weekly).
1876 – The first newspaper in Afrikaans, Die Afrikaanse Patriot, is published in Paarl.
1889 – The Coca-Cola Company, then known as the Pemberton Medicine Company, is incorporated in Atlanta.
1892 – James Naismith publishes the rules of basketball.
1908 – The Alpha Kappa Alpha sorority becomes the first Greek-letter organization founded and established by African American college women.
1910 – Construction ends on the Buffalo Bill Dam in Wyoming, United States, which was the highest dam in the world at the time, at 325 ft (99 m).
1919 – Rosa Luxemburg and Karl Liebknecht, two of the most prominent socialists in Germany, are tortured and murdered by the Freikorps at the end of the Spartacist uprising.
1919 – Great Molasses Flood: A wave of molasses released from an exploding storage tank sweeps through Boston, Massachusetts, killing 21 and injuring 150.
1934 – The 8.0 Mw Nepal–Bihar earthquake strikes Nepal and Bihar with a maximum Mercalli intensity of XI (Extreme), killing an estimated 6,000–10,700 people.
1936 – The first building to be completely covered in glass, built for the Owens-Illinois Glass Company, is completed in Toledo, Ohio.
1937 – Spanish Civil War: Nationalists and Republican both withdraw after suffering heavy losses, ending the Second Battle of the Corunna Road.
1943 – World War II: The Soviet counter-offensive at Voronezh begins.
1943 – The Pentagon is dedicated in Arlington, Virginia.
1947 – The Black Dahlia murder: the dismembered corpse of Elizabeth Short was found in Los Angeles.
1949 – Chinese Civil War: The Communist forces take over Tianjin from the Nationalist Government.
1962 – The Derveni papyrus, Europe's oldest surviving manuscript dating to 340 BC, is found in northern Greece.
1962 – Netherlands New Guinea Conflict: Indonesian Navy fast patrol boat RI Macan Tutul commanded by Commodore Yos Sudarso sunk in Arafura Sea by the Dutch Navy.
1966 – The First Nigerian Republic, led by Abubakar Tafawa Balewa is overthrown in a military coup d'état.
1967 – The first Super Bowl is played in Los Angeles. The Green Bay Packers defeat the Kansas City Chiefs 35–10.
1969 – The Soviet Union launches Soyuz 5.
1970 – Nigerian Civil War: Biafran rebels surrender following an unsuccessful 32-month fight for independence from Nigeria.
1970 – Muammar Gaddafi is proclaimed premier of Libya.
1973 – Vietnam War: Citing progress in peace negotiations, President Richard Nixon announces the suspension of offensive action in North Vietnam.
1975 – The Alvor Agreement is signed, ending the Angolan War of Independence and giving Angola independence from Portugal.
1976 – Gerald Ford's would-be assassin, Sara Jane Moore, is sentenced to life in prison.
1981 – Pope John Paul II receives a delegation from Solidarity (Polish trade union) at the Vatican led by Lech Wałęsa.
1991 – The United Nations deadline for the withdrawal of Iraqi forces from occupied Kuwait expires, preparing the way for the start of Operation Desert Storm.
1991 – Elizabeth II, in her capacity as Queen of Australia, signs letters patent allowing Australia to become the first Commonwealth realm to institute its own Victoria Cross in its honours system.
2001 – Wikipedia, a free wiki content encyclopedia, goes online.
2005 – ESA's SMART-1 lunar orbiter discovers elements such as calcium, aluminum, silicon, iron, and other surface elements on the Moon.
2007 – Barzan Ibrahim al-Tikriti, former Iraqi intelligence chief and half-brother of Saddam Hussein, and Awad Hamed al-Bandar, former chief judge of the Revolutionary Court, are executed by hanging in Iraq.
2009 – Captain Sully (Chesley Sullenberger) emergency landed a US Airways Flight 1549 in the Hudson River saving all 155 passengers after the plane collided with birds few minutes after take-off.
2013 – A train carrying Egyptian Army recruits derails near Giza, Greater Cairo, killing 19 and injuring 120 others.
2016 – The Kenyan Army suffers it worst defeat ever in a battle with Al-Shabaab Islamic insurgents in El-Adde, Somalia. An estimated 150 soldiers die.


