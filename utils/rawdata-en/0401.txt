286 – Emperor Diocletian elevates his general Maximian to co-emperor with the rank of Augustus and gives him control over the Western regions of the Roman Empire.
325 – Crown Prince Jin Chengdi, age 4, succeeds his father Jin Mingdi as emperor of the Eastern Jin dynasty.
457 – Majorian is acclaimed emperor by the Roman army after defeating 900 Alemanni near Lake Maggiore (Italy).
527 – Byzantine Emperor Justin I names his nephew Justinian I as co-ruler and successor to the throne.
528 – The daughter of Emperor Xiaoming of Northern Wei was made the "Emperor" as a male heir of the late emperor by Empress Dowager Hu, deposed and replaced by Yuan Zhao the next day; she was the first female monarch in the History of China, but not widely recognised.
1293 – Robert Winchelsey leaves England for Rome, to be consecrated as Archbishop of Canterbury.
1318 – Berwick-upon-Tweed is captured by Scotland from England.
1340 – Niels Ebbesen kills Gerhard III, Count of Holstein-Rendsburg in his bedroom, ending the 1332-1340 interregnum in Denmark.
1545 – Potosí is founded after the discovery of huge silver deposits in the area.
1572 – In the Eighty Years' War, the Watergeuzen capture Brielle from the Seventeen Provinces, gaining the first foothold on land for what would become the Dutch Republic.
1625 – A combined Spanish and Portuguese fleet of 52 ships commences the recapture of Bahia from the Dutch during the Dutch–Portuguese War.
1789 – In New York City, the United States House of Representatives holds its first quorum and elects Frederick Muhlenberg of Pennsylvania as its first Speaker.
1826 – Samuel Morey received a patent for a compressionless "Gas or Vapor Engine".
1833 – The Convention of 1833, a political gathering of settlers in Mexican Texas to help draft a series of petitions to the Mexican government, begins in San Felipe de Austin
1854 – Charles Dickens' novel Hard Times begins serialisation in his magazine Household Words.
1865 – American Civil War: Union troops led by Philip Sheridan decisively defeat Confederate troops led by George Pickett, cutting the Army of Northern Virginia's last supply line.
1867 – Singapore becomes a British crown colony.
1873 – The White Star steamer RMS Atlantic sinks off Nova Scotia, killing 547 in one of the worst marine disasters of the 19th century.
1889 – The University of Northern Colorado was established, as the Colorado State Normal School.
1891 – The Wrigley Company is founded in Chicago, Illinois.
1893 – The rank of Chief Petty Officer in the United States Navy is established.
1908 – The Territorial Force (renamed Territorial Army in 1920) is formed as a volunteer reserve component of the British Army.
1918 – The Royal Air Force is created by the merger of the Royal Flying Corps and the Royal Naval Air Service.
1924 – Adolf Hitler is sentenced to five years imprisonment for his participation in the "Beer Hall Putsch" but spends only nine months in jail.
1924 – The Royal Canadian Air Force is formed.
1933 – The recently elected Nazis under Julius Streicher organize a one-day boycott of all Jewish-owned businesses in Germany, ushering in a series of anti-Semitic acts.
1935 – India's central banking institution, The Reserve Bank of India is formed.
1937 – Aden becomes a British crown colony.
1937 – Spanish Civil War: Jaén, Spain is bombed by German fascist forces, supporting Francoist Nationalists.
1939 – Spanish Civil War: Generalísimo Francisco Franco of the Spanish State announces the end of the Spanish Civil War, when the last of the Republican forces surrender.
1941 – Fântâna Albă massacre: Between 200 and 2,000 Romanian civilians are killed by Soviet Border Troops.
1941 – A military coup in Iraq overthrows the regime of 'Abd al-Ilah and installs Rashid Ali al-Gaylani as Prime Minister.
1944 – Navigation errors lead to an accidental American bombing of the Swiss city of Schaffhausen.
1945 – World War II: The Tenth United States Army attacks the Thirty-Second Japanese Army on Okinawa.
1946 – The 8.6 Mw Aleutian Islands earthquake shakes the Aleutian Islands with a maximum Mercalli intensity of VI (Strong). A destructive tsunami reaches the Hawaiian Islands resulting in dozens of deaths, mostly in Hilo, Hawaii.
1947 – The only mutiny in the history of the Royal New Zealand Navy begins.
1948 – Communist forces respond to the introduction of the Deutsche Mark by attempting to force the western powers to withdraw from Berlin.
1948 – Faroe Islands gain autonomy from Denmark.
1949 – Chinese Civil War: The Chinese Communist Party holds unsuccessful peace talks with the Nationalist Party in Beijing, after three years of fighting.
1949 – The Government of Canada repeals Japanese-Canadian internment after seven years.
1954 – United States President Dwight D. Eisenhower authorizes the creation of the United States Air Force Academy in Colorado.
1955 – The EOKA rebellion against the British Empire begins in Cyprus, with the goal of unifying with Greece.
1960 – The TIROS-1 satellite transmits the first television picture from space.
1970 – President Richard Nixon signs the Public Health Cigarette Smoking Act into law, requiring the Surgeon General's warnings on tobacco products and banning cigarette advertising on television and radio in the United States, effective 1 January 1971.
1971 – Bangladesh Liberation War: The Pakistan Army massacre over 1,000 people in Keraniganj Upazila, Bangladesh.
1973 – Project Tiger, a tiger conservation project, is launched in the Jim Corbett National Park, India.
1974 – The Local Government Act 1972 of England and Wales comes into effect.
1976 – Apple Inc. is formed by Steve Jobs, Steve Wozniak, and Ronald Wayne in Cupertino, California, USA.
1978 – The Philippine College of Commerce, through a presidential decree, becomes the Polytechnic University of the Philippines.
1979 – Iran becomes an Islamic republic by a 99% vote, officially overthrowing the Shah.
1986 – Communist Party of Nepal (Mashal) cadres attack a number of police stations in Kathmandu, seeking to incite a popular rebellion.
1989 – Margaret Thatcher's new local government tax, the Community Charge (commonly known as the "poll tax"), is introduced in Scotland.
1997 – Comet Hale–Bopp is seen passing at perihelion.
1999 – Nunavut is established as a Canadian territory carved out of the eastern part of the Northwest Territories.
2001 – An EP-3E United States Navy surveillance aircraft collides with a Chinese People's Liberation Army Shenyang J-8 fighter jet. The Navy crew makes an emergency landing in Hainan, China and is detained.
2001 – Former President of Federal Republic of Yugoslavia Slobodan Milošević surrenders to police special forces, to be tried on war crimes charges.
2001 – Same-sex marriage becomes legal in the Netherlands, the first contemporary country to allow it.
2004 – Google announces Gmail to the public.
2011 – After protests against the burning of the Quran turn violent, a mob attacks a United Nations compound in Mazar-i-Sharif, Afghanistan, resulting in the deaths of thirteen people, including eight foreign workers.
2016 – Nagorno-Karabakh clashes: The Four Day War or April War, began along the Nagorno-Karabakh line of contact on April 1.


