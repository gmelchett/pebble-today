/*
 * Copyright (c) Jonas Aaberg <cja@lithops.se> 2017
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <pebble.h>
#include "today.h"
#include "tinf.h"

#ifdef PBL_COLOR
#include "data_basalt.inc"
#else
#include "data_aplite.inc"
#endif

static ResHandle curr_reshandle;

static unsigned char read_byte(TINF_DATA *d)
{
	unsigned char b;

	if (d->source_ptr % SOURCE_SIZE == 0)
		resource_load_byte_range(curr_reshandle, d->source_ptr, d->source_buff, SOURCE_SIZE);
	b = d->source_buff[d->source_ptr % SOURCE_SIZE];
	d->source_ptr++;
	return b;
}

static int yearday(int month, int day)
{
	static const char month_lengths[] = {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
	int n = 0;

	for (int i = 0; i < month; i++) 
		n += month_lengths[i];
	return n + day - 1;
}

static void fix_year(bool bc, int *oi, bool *space_found) 
{
	if (*space_found)
		return;

	if (bc) {
		for (int k = 0; bc_string[k] != '\0'; k++, (*oi)++)
			out_buffer[(*oi)] = bc_string[k];
	} 
	out_buffer[(*oi)] = ' ';
	(*oi)++;
	out_buffer[(*oi)] = '-';
	(*oi)++;
	out_buffer[(*oi)] = ' ';
	(*oi)++;

	(*space_found) = true;
}

/* month 0 = January */
int today_get_num_events(int month, int day) 
{
	int yd = yearday(month, day);
	return whereis[yd].num_events;
}

static void add_day(int day, int *oi)
{
	if (day > 9) {
		date_buffer[*oi] = (char)(day / 10) + '0';
		(*oi)++;
	}
	date_buffer[*oi] = (char)(day % 10) + '0';
	date_buffer[(*oi) + 1] = ' ';
	(*oi) += 2;
}

static void add_month(int month, int *oi)
{
	for (int i = 0; month_strings[month][i] != '\0'; i++) {
		date_buffer[*oi] = month_strings[month][i];
		(*oi)++;
	}
	date_buffer[*oi] = ' ';
	(*oi)++;
}

char *today_get_date_as_str(int month, int day)
{
	int oi = 0;

	if (day_before_month) {
		add_day(day, &oi);
		add_month(month, &oi);
	} else {
		add_month(month, &oi);
		add_day(day, &oi);
	}

	out_buffer[oi - 1] = '\0';

	return date_buffer;
}

static TINF_DATA data;
char *today(int month, int day, int n)
{
	int yd;
	int oi = 0;
	int c = 0;
	int p;
	bool bc;
	bool space_found = false;

	yd = yearday(month, day);

	if (whereis[yd].num_events == 0)
		return (char *)noevent_string;

	if (n >= whereis[yd].num_events)
		return "ERROR: No such event";

	uzlib_init();
	memset(&data, 0, sizeof(TINF_DATA));
	uzlib_uncompress_init(&data, NULL, 0);
	data.source = NULL;
	data.readSource = read_byte;
	data.dest = decompress_buffer;

	curr_reshandle = resource_get_handle(whereis[yd].res_idx);

	uzlib_uncompress(&data);

	for (p = whereis[yd].offset; p < uncompressed_res_size[whereis[yd].res_idx - RESOURCE_ID_EVENT_000]; p++) {
		if (c == n)
			break;
		if (decompress_buffer[p] == '\n')
			c++;
	}

	if (decompress_buffer[p] == '-') {
		p++;
		bc = true;
	} else {
		bc = false;
	}

	for (int i = p; decompress_buffer[i] !='\n'; i++) {

		if (decompress_buffer[i] == '#' ||
		    decompress_buffer[i] == '}' ||
		    decompress_buffer[i] == '{' ||
		    decompress_buffer[i] == '@') {
			int idx = 0;
			i++;
			switch(decompress_buffer[i-1]) {
			case '@':
				idx += (int)(decompress_buffer[i] - '0')*1000;
				i++;
				__attribute__ ((fallthrough));
			case '{':
				idx += (int)(decompress_buffer[i] - '0')*100;
				i++;
				__attribute__ ((fallthrough));
			case '}':
				idx += (int)(decompress_buffer[i] - '0')*10;
				i++;
				__attribute__ ((fallthrough));
			case '#':
				idx += (int)(decompress_buffer[i] - '0');
				break;
			default:
				break;
			}

			int off = dict_offsets[idx];
			int ds = dict_offsets[idx + 1] - dict_offsets[idx];
			for (int j = 0; j < ds; j++) {

				if (dict[off + j] == ' ' && !space_found) {
					fix_year(bc, &oi, &space_found);
				} else {
					out_buffer[oi] = dict[off + j];
					oi++;
				}

				if (dict[off+j] == ',' || dict[off + j] == ',') {
					out_buffer[oi] = ' ';
					oi++;
				}
			}
			continue;
		} else 

		if (decompress_buffer[i] == ' ' && !space_found) {
			fix_year(bc, &oi, &space_found);
			continue;
		}

		out_buffer[oi] = decompress_buffer[i];
		oi++;

		if (decompress_buffer[i] == '.' || decompress_buffer[i] == ',') {
			out_buffer[oi] = ' ';
			oi++;
		}
	}
	out_buffer[oi] = '.';
	oi++;
	out_buffer[oi] = '\0';
	return out_buffer;
}
