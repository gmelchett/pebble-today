/*
 * Copyright (c) Jonas Aaberg <cja@lithops.se> 2017
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <pebble.h>
#include "today.h"

#ifdef PBL_COLOR
#include "about_basalt.inc"
#else
#include "about_aplite.inc"
#endif

static Window *s_main_window;
static Window *s_about_window;
static ScrollLayer *s_main_scroll_layer;
static ScrollLayer *s_about_scroll_layer;
static TextLayer *s_main_msg_layer;
static TextLayer *s_main_title_layer;
static TextLayer *s_about_title_layer;

static TextLayer *s_about_subj_text[NUM_SUBJ];
static TextLayer *s_about_val_text[NUM_SUBJ];
static AppTimer *s_timer = NULL;

static int s_current_event;
static time_t s_when;

#define TITLE_HEIGHT 30
#define TEXT_HEIGHT 20

#define TODAY_TIMEOUT 60*1000 /* ms */

static void today_timeout_cb(void *data)
{
	(void) window_stack_pop(false);
}

static void main_window_change_msg(ClickRecognizerRef recognizer, void *context)
{
	struct tm *tm = localtime(&s_when);
	GPoint p = {.x = 0, .y = 0};

	s_current_event = (s_current_event + 1) % today_get_num_events(tm->tm_mon, tm->tm_mday);

	text_layer_set_text(s_main_title_layer, today_get_date_as_str(tm->tm_mon, tm->tm_mday));
	text_layer_set_text(s_main_msg_layer, today(tm->tm_mon, tm->tm_mday, s_current_event));
	scroll_layer_set_content_offset(s_main_scroll_layer, p, false);

	if (s_timer)
		app_timer_reschedule(s_timer, TODAY_TIMEOUT);
	else
		s_timer = app_timer_register(TODAY_TIMEOUT, today_timeout_cb, NULL);

	GSize msg_size = text_layer_get_content_size(s_main_msg_layer);
	msg_size.h += TITLE_HEIGHT + 6;

	scroll_layer_set_content_size(s_main_scroll_layer, msg_size);
}

static void main_window_push_about(ClickRecognizerRef recognizer, void *context)
{
	window_stack_push(s_about_window, false);
}

static void main_window_prev_day(ClickRecognizerRef recognizer, void *context)
{
	s_when -= 24*60*60;
	main_window_change_msg(recognizer, context);
}

static void main_window_next_day(ClickRecognizerRef recognizer, void *context)
{
	if (s_when + 24*60*60 < time(NULL))
		s_when += 24*60*60;
	main_window_change_msg(recognizer, context);
}

static void main_window_click_config_provider(void *context) 
{
	window_single_click_subscribe(BUTTON_ID_SELECT, main_window_change_msg);
	window_multi_click_subscribe(BUTTON_ID_SELECT, 2, 0, 0, true, main_window_push_about);
	window_multi_click_subscribe(BUTTON_ID_UP, 2, 0, 0, true, main_window_prev_day);
	window_multi_click_subscribe(BUTTON_ID_DOWN, 2, 0, 0, true, main_window_next_day);
}

static void about_window_close(ClickRecognizerRef recognizer, void *context)
{
	(void) window_stack_pop(false);
}

static void about_window_click_config_provider(void *context) 
{
	window_single_click_subscribe(BUTTON_ID_SELECT, about_window_close);
}

static void about_window_load(Window *window)
{
	Layer *window_layer = window_get_root_layer(window);
	GRect bounds = layer_get_frame(window_layer);

	GRect title_bounds = GRect(0, 0, bounds.size.w, TITLE_HEIGHT);
	GRect subj_bounds = GRect(0, TITLE_HEIGHT, bounds.size.w, TEXT_HEIGHT);
	GRect val_bounds = GRect(0, TITLE_HEIGHT+TEXT_HEIGHT, bounds.size.w, TEXT_HEIGHT);
	GSize total_size = {.h = TITLE_HEIGHT, .w = bounds.size.w};
	s_about_scroll_layer = scroll_layer_create(bounds);

	scroll_layer_set_click_config_onto_window(s_about_scroll_layer, window);

#ifdef PBL_ROUND
	scroll_layer_set_paging(s_about_scroll_layer, true);
#endif

	s_about_title_layer = text_layer_create(title_bounds);

	text_layer_set_font(s_about_title_layer, fonts_get_system_font(FONT_KEY_GOTHIC_24_BOLD));
	text_layer_set_text_color(s_about_title_layer, GColorWhite);
#ifdef PBL_COLOR
	text_layer_set_background_color(s_about_title_layer, GColorPictonBlue); //GColorVividCerulean);
#else
	text_layer_set_background_color(s_about_title_layer, GColorBlack);
#endif
	text_layer_set_text_alignment(s_about_title_layer, GTextAlignmentCenter);
	text_layer_set_text(s_about_title_layer, "Today in History");

	scroll_layer_add_child(s_about_scroll_layer, text_layer_get_layer(s_about_title_layer));

	for (int i = 0; i < NUM_SUBJ; i++) {
		s_about_subj_text[i] = text_layer_create(subj_bounds);
		s_about_val_text[i] = text_layer_create(val_bounds);
		text_layer_set_font(s_about_subj_text[i], fonts_get_system_font(FONT_KEY_GOTHIC_14_BOLD));
		text_layer_set_font(s_about_val_text[i], fonts_get_system_font(FONT_KEY_GOTHIC_14));
		text_layer_set_text(s_about_subj_text[i], subj_title[i]);
		text_layer_set_text(s_about_val_text[i], subj_val[i]);

		scroll_layer_add_child(s_about_scroll_layer, text_layer_get_layer(s_about_subj_text[i]));
		scroll_layer_add_child(s_about_scroll_layer, text_layer_get_layer(s_about_val_text[i]));

		subj_bounds.origin.y += 2*TEXT_HEIGHT;
		val_bounds.origin.y += 2*TEXT_HEIGHT;

		total_size.h += 2 * TEXT_HEIGHT;
	}
	scroll_layer_set_content_size(s_about_scroll_layer, total_size);

	layer_add_child(window_layer, scroll_layer_get_layer(s_about_scroll_layer));

	ScrollLayerCallbacks callbacks = {
		.click_config_provider = about_window_click_config_provider,
	};
	scroll_layer_set_callbacks(s_about_scroll_layer, callbacks);
}

static void about_window_unload(Window *window)
{
	for (int i = 0; i < NUM_SUBJ; i++) {
		text_layer_destroy(s_about_subj_text[i]);
		text_layer_destroy(s_about_val_text[i]);
	}
	text_layer_destroy(s_about_title_layer);
	scroll_layer_destroy(s_about_scroll_layer);
}

static void main_window_load(Window *window) 
{
	Layer *window_layer = window_get_root_layer(window);
	GRect bounds = layer_get_frame(window_layer);
	GRect max_menu_bounds = GRect(0, 0, bounds.size.w, 30);
	GRect max_text_bounds = GRect(0, 30, bounds.size.w, 2000);

	struct tm *tm = localtime(&s_when);

	s_main_scroll_layer = scroll_layer_create(bounds);

	scroll_layer_set_click_config_onto_window(s_main_scroll_layer, window);

#ifdef PBL_ROUND
	scroll_layer_set_paging(s_main_scroll_layer, true);
#endif

	s_main_title_layer = text_layer_create(max_menu_bounds);
	text_layer_set_font(s_main_title_layer, fonts_get_system_font(FONT_KEY_GOTHIC_24_BOLD));
	text_layer_set_text_color(s_main_title_layer, GColorWhite);
#ifdef PBL_COLOR
	text_layer_set_background_color(s_main_title_layer, GColorPictonBlue); //GColorVividCerulean);
#else
	text_layer_set_background_color(s_main_title_layer, GColorBlack);
#endif
	text_layer_set_text_alignment(s_main_title_layer, GTextAlignmentCenter);
	text_layer_set_text(s_main_title_layer, today_get_date_as_str(tm->tm_mon, tm->tm_mday));
	GSize menu_size = text_layer_get_content_size(s_main_title_layer);

	scroll_layer_add_child(s_main_scroll_layer, text_layer_get_layer(s_main_title_layer));

	max_text_bounds.origin.y = menu_size.h + 6;

	s_main_msg_layer = text_layer_create(max_text_bounds);

	s_current_event = rand() % today_get_num_events(tm->tm_mon, tm->tm_mday);

	text_layer_set_text(s_main_msg_layer, today(tm->tm_mon, tm->tm_mday, s_current_event));

	text_layer_set_font(s_main_msg_layer, fonts_get_system_font(FONT_KEY_GOTHIC_18));

	scroll_layer_add_child(s_main_scroll_layer, text_layer_get_layer(s_main_msg_layer));

	GSize msg_size = text_layer_get_content_size(s_main_msg_layer);
	msg_size.h += TITLE_HEIGHT + 6;

	scroll_layer_set_content_size(s_main_scroll_layer, msg_size);

	layer_add_child(window_layer, scroll_layer_get_layer(s_main_scroll_layer));

#ifdef PBL_ROUND
	text_layer_set_text_alignment(s_main_msg_layer, GTextAlignmentCenter);
	uint8_t inset = 4;
	text_layer_enable_screen_text_flow_and_paging(s_main_msg_layer, inset);
#endif
	ScrollLayerCallbacks callbacks = {
		.click_config_provider = main_window_click_config_provider,
	};
	scroll_layer_set_callbacks(s_main_scroll_layer, callbacks);

	if (s_timer)
		app_timer_reschedule(s_timer, TODAY_TIMEOUT);
	else
		s_timer = app_timer_register(TODAY_TIMEOUT, today_timeout_cb, NULL);
}

static void main_window_unload(Window *window)
{
	if (s_timer)
		app_timer_cancel(s_timer);
	s_timer = NULL;

	text_layer_destroy(s_main_msg_layer);
	text_layer_destroy(s_main_title_layer);
	scroll_layer_destroy(s_main_scroll_layer);
}

static void init(void)
{
	s_main_window = window_create();
	s_when = time(NULL);

	window_set_window_handlers(s_main_window, (WindowHandlers) {
			.load = main_window_load,
			.unload = main_window_unload,
			});

	s_about_window = window_create();

	window_set_window_handlers(s_about_window, (WindowHandlers) {
			.load = about_window_load,
			.unload = about_window_unload,
			});

	window_stack_push(s_main_window, false);
}

static void deinit(void)
{
	window_destroy(s_main_window);
	window_destroy(s_about_window);
}

int main(void) 
{
	init();
	app_event_loop();
	deinit();
	return 0;
}
